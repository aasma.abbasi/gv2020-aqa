package pages.itinerary;

import core.WebDriverFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.IPages.IOrder;
import pages.Page;

import java.util.List;

public class pageOrder implements IOrder {

    public pageOrder(Page page) {

    }

    //Locators:
    private By byOrderState = By.className("status");
    private By byOrderScreen = By.className("order-number");
    private By byOrderText = By.className("order-number");
    private By byHeadingText = By.className("ant-card-head-title");
    private By byOrderTable = By.className("ant-table-tbody");
    private By byPreference = By.className("preference-user");
    private By byBackArrow = By.className("anticon-arrow-left");
    private By byInput = By.tagName("input");
    private By byOperationalNoteList = By.className("opt-change");
    private By byType = By.id("absolute-type");
    private By byLabel = By.tagName("label");
    private By bySpan = By.tagName("span");
    private By byItineraryList = By.id("itinerary-tabs");
    private By byPDFAttachment = By.className("attachments"); //("anticon-paper-clip");
    private By byToField = By.className("subscribers-list");
    private By byEmailSubjectField = By.className("mail-subject");
    private By byEmailBodyField = By.className("mail-body");
    private By byStatus = By.className("status");
    private By byEditCopy = By.id("itinerary-edit-copy");
    private By byItineraryTypes = By.className("ant-radio-group");


    @Override
    public List<WebElement> getOrderState() {
        return (List<WebElement>) WebDriverFactory.getDriver().findElements(byOrderState);
    }

    @Override
    public WebElement getOrderScreen() {
        return WebDriverFactory.getDriver().findElement(byOrderScreen);
    }


    //Functions

    public WebElement getOrderText() {
        return WebDriverFactory.getDriver().findElement(byOrderText);
    }

    public int getTotalOrderCount() {
       return WebDriverFactory.getDriver().findElement(byOrderTable).findElements(By.tagName("tr")).size();
    }

    public WebElement getOrderHeading() {
        return WebDriverFactory.getDriver().findElements(byHeadingText).get(0);
    }

    public WebElement getToField() {
        return WebDriverFactory.getDriver().findElement(byToField);
    }

    public WebElement getEmailSubjectField() {
        return WebDriverFactory.getDriver().findElement(byEmailSubjectField);
    }

    public WebElement getEmailBodyField() {
        return WebDriverFactory.getDriver().findElement(byEmailBodyField);
    }

    public WebElement clickEditCopy() {
        return WebDriverFactory.getDriver().findElement(byEditCopy);
    }

    int index = 0;

    public void getItineraryType(String type) {

        index = 0;
        List<WebElement> tableItinerary = WebDriverFactory.getDriver().findElements(By.className("desc-row"));
        for (int i = 0; i < tableItinerary.size(); i++) {
            if (tableItinerary.get(i).findElement(byStatus).getText().equals(type)) {
                index = i;
                break;
            }
        }
        tableItinerary.get(index).click();
    }

    public Boolean checkPreferenceApplied() {
        return WebDriverFactory.getDriver().findElements(byPreference).get(index).getText().isEmpty();
    }

    public void deselectLeg() throws InterruptedException {
        //select all legs
        pageItinerary.checkMultipleLegs().click();
        //deselect first leg
        pageItinerary.deselectFirstLeg().click();
    }

    public WebElement backArrow() {
        return WebDriverFactory.getDriver().findElement(byBackArrow);
    }

    public void checkDeselectLeg() {
        Assert.assertFalse(pageItinerary.deselectFirstLeg().findElement(byInput).isSelected());
    }

    public WebElement deselectOperationalNote () {
        //switch off operational note of first leg
        return WebDriverFactory.getDriver().findElements(byOperationalNoteList).get(0);
    }

    public void checkDeselectedOperationalNote() {
        Assert.assertFalse(deselectOperationalNote().getAttribute("class").contains("checked"));
    }

    public static String oldItineraryType = "";
    public static String newItineraryType = "";

    public String checkItineraryTypeSelected() {
        List<WebElement> bodyLabels = WebDriverFactory.getDriver().findElements(byItineraryTypes).get(0).findElements(byLabel);

        for (int i = 0; i < bodyLabels.size(); i++) {
            if (bodyLabels.get(i).getAttribute("className").contains("checked")) {
                newItineraryType = bodyLabels.get(i).getText();
            }
        }

        return newItineraryType;
    }

    public String[] updateItineraryType() {
        WebElement update = null;
        String isChecked = "";
        List<WebElement> bodyLabels = WebDriverFactory.getDriver().findElements(byItineraryTypes).get(0).findElements(byLabel);

        for (int i = 0; i < bodyLabels.size(); i++){
            isChecked = bodyLabels.get(i).getAttribute("className");
            if(!isChecked.contains("checked")) {
                update = bodyLabels.get(i);
                newItineraryType = bodyLabels.get(i).getText();
            }
            else if (isChecked.contains("checked")){
                oldItineraryType = bodyLabels.get(i).getText();
            }
        }
        update.click();

        return new String[] {oldItineraryType, newItineraryType};
    }

    public void getInitialTypeItinerary() {
        //select Initial
        WebDriverFactory.getDriver().findElement(byType).findElements(byLabel).get(1).click();
        //select 1st itinerary
        WebDriverFactory.getDriver().findElements(byItineraryList).get(0).click();
    }

    public void getRevisedTypeItinerary() {
        //select Revised
        WebDriverFactory.getDriver().findElement(byType).findElements(byLabel).get(2).click();
        //select 1st itinerary
        WebDriverFactory.getDriver().findElements(byItineraryList).get(0).click();
    }

    public void getFinalTypeItinerary() {
        //select Final
        WebDriverFactory.getDriver().findElement(byType).findElements(byLabel).get(3).click();
        //select 1st itinerary
        WebDriverFactory.getDriver().findElements(byItineraryList).get(0).click();
    }

    public WebElement getAttachedPdf() {
        return WebDriverFactory.getDriver().findElement(byPDFAttachment);
    }

    public void selectAdditionalItems() throws InterruptedException {
        //only first checked. all others unchecked
        if(!pageItinerary.getCrewCheckBox().isSelected() )         {pageItinerary.getCrewCheckBox().click();}
        if(pageItinerary.getTailNumberCheckBox().isSelected())     {pageItinerary.getTailNumberCheckBox().click();}
        if(pageItinerary.getCallsignCheckBox().isSelected())       {pageItinerary.getCallsignCheckBox().click();}
        if(pageItinerary.getHanderAddressCheckBox().isSelected() ) {pageItinerary.getHanderAddressCheckBox().click();}
        if(pageItinerary.getActualACCheckBox().isSelected())       {pageItinerary.getActualACCheckBox().click();}

        Thread.sleep(5000);
    }

    public void checkAdditionalItems() {
        boolean check = pageItinerary.getCrewCheckBox().isSelected() &&
                !pageItinerary.getTailNumberCheckBox().isSelected() &&
                !pageItinerary.getCallsignCheckBox().isSelected() &&
                !pageItinerary.getHanderAddressCheckBox().isSelected() &&
                !pageItinerary.getActualACCheckBox().isSelected();

        Assert.assertTrue(check);
    }

    public void enterOrderId(String orderId) {
        WebDriverFactory.getDriver().findElement(By.id("error")).sendKeys(orderId);
        WebDriverFactory.getDriver().findElement(By.className("ant-input-search-button")).click();
    }
}
