package pages.itinerary;

import core.WebDriverFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.IPages.IEmail;
import pages.Page;

import java.util.List;

import static pages.itinerary.pageItinerary.legsCount;
import static pages.itinerary.pageItinerary.selectedCustomItems;

public class pageEmail implements IEmail{

    public pageEmail(Page page){
    }

    int viewsCount = 0;
    private By byItineraryType = By.cssSelector(".ant-col-2:nth-child(2) .col-val");
    private By byEmailNotification = By.className("ant-notification-notice-message");
    private By byEmailBody = By.className("mail-body");

    private static By byTr = By.tagName("tr");
    private static By byTh = By.tagName("th");
    private static By byTd = By.tagName("td");
    private static By bySpan = By.tagName("span");
    private static By byLabel = By.tagName("label");
    private static By byTable = By.tagName("table");

    public void viewsSize() {
        List <WebElement> views = WebDriverFactory.getDriver().findElement(By.className("flight-summary-card")).findElements(By.linkText("View"));
        viewsCount = views.size();

        Assert.assertTrue(viewsCount == legsCount);
    }

    public void viewAgainstEachLeg() throws InterruptedException {
        List <WebElement> views = WebDriverFactory.getDriver().findElement(By.className("flight-summary-card")).findElements(By.linkText("View"));
        viewsCount = views.size();

        for (int i = 0; i < viewsCount; i++) {
            Thread.sleep(5000);

            views.get(i).click();

            List <WebElement> additionalItems =  WebDriverFactory.getDriver().findElements(By.tagName("li"));

            Thread.sleep(5000);

            Assert.assertTrue(additionalItems.get(0).getText().equals(selectedCustomItems[0]));
            Assert.assertTrue(additionalItems.get(1).getText().equals(selectedCustomItems[1]));

            WebDriverFactory.getDriver().findElement(By.id("catering-pop-up-btn")).click();
        }
    }

    public WebElement checkItineraryType() {
        return WebDriverFactory.getDriver().findElement(byItineraryType);
    }

    public WebElement checkEmailBody() {
        WebElement bodyFormat = WebDriverFactory.getDriver().findElement(byEmailBody);
        return bodyFormat.findElement(byTable);
    }

    public By checkEmailNotification() {
        return byEmailNotification;
    }

    public WebElement checkCateringMessageText() {
        WebElement table = WebDriverFactory.getDriver().findElements(byTable).get(0);
        WebElement cateringRow = table.findElement(By.tagName("tbody")).findElements(byTr).get(1).findElements(byTd).get(1).findElement(bySpan);
        return cateringRow;
    }



}
