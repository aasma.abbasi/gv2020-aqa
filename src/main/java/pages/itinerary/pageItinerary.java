package pages.itinerary;

import core.WebDriverFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.IPages.IItinerary;
import pages.Page;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class pageItinerary implements IItinerary{

    public pageItinerary(Page page) throws AWTException {
    }

    //Locators
    private static By byFirstLeg = By.className("ant-table-tbody");
    private By byCreateItineraryButton = By.id("create-itinerary-btn");
    private By byItineraryType = By.cssSelector("label.type-3.ant-radio-button-wrapper > span:nth-child(2)");
    private By bySelectAllCheckBox = By.cssSelector("span > div > label > span > input");
    private By byPreferenceDropDown = By.className("ant-select-selection__rendered");
    private By byPreferenceList = By.className("ant-select-dropdown-menu-item");
    private By byEmailItineraryButton = By.cssSelector("div.ant-col-6.ant-col-offset-16 > button:nth-child(2)");
    private By byDeselectLegCheckBox = By.cssSelector(".ant-table-tbody tr:nth-child(2) input");
    private static By byTableHeader = By.className("ant-table-thead");
    private By byBespokeCheck = By.id("catering-message-2");
    private By byCheckEdit = By.className("action-items");
    private By byHeadingText = By.className("ant-card-head-title");
    private By byPreferenceListDD = By.className("ant-select-selection--single");
//    private By byPreferenceList = By.className("ant-select-dropdown-menu-item");
    private By byResolve = By.className("margin-right-10");
 //   private By byCreateItineraryButton = By.className("margin-right-10");
    private By bySelectAdd = By.className("add-email-tag");
    private By byAdd = By.cssSelector("#add-email-tag > input");
    private By bySaveAndExit = By.className("ant-btn-primary");
    private By byCheckList = By.className("subscribers-list");
    private static By byCrewCheckBox = By.cssSelector("td:nth-child(2) > div > label:nth-child(1) > span.ant-checkbox > input");
    private static By byTailNumberCheckBox = By.cssSelector("label:nth-child(2) > span.ant-checkbox > input");
    private static By byCallsignCheckBox = By.cssSelector("label:nth-child(3) > span.ant-checkbox > input");
    private static By byHanderAddressCheckBox = By.cssSelector("label:nth-child(4) > span.ant-checkbox > input");
    private static By byActualACCheckBox = By.cssSelector("label:nth-child(5) > span.ant-checkbox > input");
    private By byPetCheckBox = By.cssSelector("label:nth-child(6) > span.ant-checkbox > input");
    private By byFinalType = By.cssSelector("label.type-3.ant-radio-button-wrapper");
    private By byFinalText = By.cssSelector("#itinerary-content > div > div.ant-card-body > div:nth-child(1) > div:nth-child(2) > p.col-val");
    private By byUploadFile = By.id("upload-btn");
    private By byUploadFileIcon = By.className("anticon-upload");
    private By byUploadFilePrompt = By.className("ant-message-success");
    private By byUploadFilePromptColour = By.className("anticon-check-circle");

    private By byFlightCentre = By.id("view-order-details");
    private By byType1 = By.className("type-1");
    private By byType2 = By.className("type-2");
    private By byType3 = By.className("type-3");
    private By byTableHead = By.className("ant-table-thead");
    private By byTableBody = By.className("ant-table-tbody");
    private static By byTr = By.tagName("tr");
    private static By byTh = By.tagName("th");
    private static By byTd = By.tagName("td");
    private static By bySpan = By.tagName("span");
    private static By byLabel = By.tagName("label");

    private By byItineraryModulePage = By.cssSelector(".ant-breadcrumb-link a");
    private By bySearchItineraryScreen = By.cssSelector(".search-box h1");
    private By bySearchOptionSelectedItem = By.className("ant-select-selection-selected-value");
    private By byGotoButton = By.cssSelector(".ant-input-suffix button");
    private By bySearchField = By.id("error");
    private By bycalendarIcon = By.className("anticon-calendar");
    private By bySearchButton = By.className("ant-btn-lg");
    private By bySearchButtonText = By.cssSelector("span > button > span");
    private By byOrderListAccountSearch = By.cssSelector(".ant-table-tbody tr");
    private By byNoOrderDisplay = By.className("ant-table-placeholder");
    private By byCalendarPicker = By.className("range-picker");

    private By byStartDateCalendar =  By.cssSelector(".ant-calendar-picker-input input");
    private By byBackButtinOnStartDate = By.className("ant-calendar-prev-month-btn");
    private By byNextMonthOnEndDate = By.cssSelector(".ant-calendar-range-left  a.ant-calendar-next-month-btn");
    private By bySelectStartDate =  By.cssSelector(".ant-calendar-range-left tr:nth-child(2) > td:nth-child(2) > div");
    private By bySelectEndDate = By.cssSelector(".ant-calendar-selected-date > div");

    private By byCustomOption = By.id("catering-message-1");
    private By byVIPOption = By.id("catering-message-3");
    private By byCustomMsgField = By.id("custom-message-text");
    private By byCateringPopUp = By.id("catering-pop-up-radio-1");
    private By byInput = By.className("ant-input");
    private By byCateringPopUpButton = By.id("catering-pop-up-btn");
    private By byGreetings = By.id("greetings");
    private By byEmailText = By.id("email-text");
    private By bySubject = By.id("subject-format");


    //Functions

    //Robot functions
    public void type(int i) throws AWTException{
        robot.delay(60);
        robot.keyPress(i);
        robot.keyRelease(i);
    }
    public void type(String s) throws AWTException{
        byte[] bytes = s.getBytes();
        for (byte b : bytes){
            int code = b;
            // keycode only handles [A-Z] (which is ASCII decimal [65-90])
            if (code > 96 && code < 123)
                code = code - 32;
            robot.delay(100);
            robot.keyPress(code);
            robot.keyRelease(code);
        }
    }


    public WebElement getStartDateCalendarField(){
        return WebDriverFactory.getDriver().findElement(byStartDateCalendar);
    }

    public WebElement getCalendarPicker(){
        return WebDriverFactory.getDriver().findElement(byCalendarPicker);
    }


    public WebElement getBackButtonOnStartDate(){
        return WebDriverFactory.getDriver().findElement(byBackButtinOnStartDate);
    }

    public WebElement getNextMonthOnEndDate(){
        return WebDriverFactory.getDriver().findElement(byNextMonthOnEndDate);
    }

    public WebElement getSelectStartDate(){
        return WebDriverFactory.getDriver().findElement(bySelectStartDate);
    }

    public WebElement getSelectEndDate(){
        return WebDriverFactory.getDriver().findElement(bySelectEndDate);
    }


    public WebElement getItineraryModulePage(){
        return WebDriverFactory.getDriver().findElement(byItineraryModulePage);
    }

    public WebElement getSearchItineraryScreen(){
        return WebDriverFactory.getDriver().findElement(bySearchItineraryScreen);
    }

    public WebElement getSearchOptionSelectedItem(){
        return WebDriverFactory.getDriver().findElement(bySearchOptionSelectedItem);
    }

    public WebElement getGotoButton(){
        return WebDriverFactory.getDriver().findElement(byGotoButton);
    }

    public WebElement getSearchField(){
        return WebDriverFactory.getDriver().findElement(bySearchField);
    }

    public WebElement getCalendarIcon(){
        return WebDriverFactory.getDriver().findElement(bycalendarIcon);
    }

    public WebElement getSearchButton(){
        return WebDriverFactory.getDriver().findElement(bySearchButton);
    }

    public WebElement getSearchButtonText(){
        return WebDriverFactory.getDriver().findElement(bySearchButtonText);
    }

    public WebElement getNoOrderDisplayText(){
        return WebDriverFactory.getDriver().findElement(byNoOrderDisplay);
    }

    public WebElement getOrderListAccountSearch(){
        return WebDriverFactory.getDriver().findElement(byOrderListAccountSearch);
    }




    @Override
    public WebElement getCreateItineraryButton(){
        return WebDriverFactory.getDriver().findElement(byCreateItineraryButton);
    }

    @Override
    public WebElement selectItineraryType(){
        return WebDriverFactory.getDriver().findElement(byItineraryType);
    }

    @Override
    public List<WebElement> clickOnResolveButton(){
        return WebDriverFactory.getDriver().findElements(byResolve);
    }

    @Override
    public WebElement selectAllCheckboxButton(){
        return WebDriverFactory.getDriver().findElement(bySelectAllCheckBox);
    }

    @Override
    public WebElement sendEmailItineraryButton(){
        return WebDriverFactory.getDriver().findElement(byEmailItineraryButton);
    }

    @Override
    public WebElement deSelectLegCheckBox(){
        return WebDriverFactory.getDriver().findElement(byDeselectLegCheckBox);
    }




    public void selectPreference() throws InterruptedException {
         WebDriverFactory.getDriver().findElement(byPreferenceDropDown).click();
 //        WebDriverFactory.getDriver().findElement(byPreferenceDropDown).sendKeys(Keys.ARROW_DOWN);
  //      WebDriverFactory.getDriver().findElement(byPreferenceDropDown).sendKeys(Keys.ENTER);

        Actions action = new Actions(WebDriverFactory.getDriver());

        Thread.sleep(3000);
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ENTER).build().perform();

    }

    public List<WebElement> getPrefenceList(){
        return (List<WebElement>) WebDriverFactory.getDriver().findElements(byPreferenceList);
    }

    public WebElement getUploadFile(){
        return WebDriverFactory.getDriver().findElement(byUploadFile);
    }

    public WebElement getUploadFileIcon(){
        return WebDriverFactory.getDriver().findElement(byUploadFileIcon);
    }

    public WebElement getUploadFilePromptText(){
        return WebDriverFactory.getDriver().findElement(byUploadFilePrompt);
    }

    public WebElement getUploadFilePromptColour(){
        return WebDriverFactory.getDriver().findElement(byUploadFilePromptColour);
    }

    Robot robot = new Robot();
    public void fileSelection() throws AWTException {
//        robot.delay(1000);
        robot.keyPress(KeyEvent.VK_C);
        robot.keyRelease(KeyEvent.VK_C);
        robot.keyPress(KeyEvent.VK_SHIFT);
        robot.keyPress(KeyEvent.VK_SEMICOLON);
        robot.keyRelease(KeyEvent.VK_SEMICOLON);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        type("\\GV2020\\dummy.pdf");
        robot.keyPress(KeyEvent.VK_ENTER);
    }

//    public WebElement checkFile(){
//        return WebDriverFactory.getDriver().findElement(byUploadFile);
//    }

    public static WebElement checkMultipleLegs() {
        WebElement tableHeader = WebDriverFactory.getDriver().findElement(byTableHeader);
        WebElement checkAll = tableHeader.findElements(byTh).get(0);
        String checkifAllSelected = checkAll.findElement(bySpan).findElement(byLabel).findElement(bySpan).getAttribute("class");
        //if all legs already checked
        if(checkifAllSelected.contains("checked")) {
            //click to deselect all the legs first
            checkAll.click();
        }
        return checkAll;
    }

    public WebElement checkBespoke() {
        return WebDriverFactory.getDriver().findElement(byBespokeCheck);
    }

    public int checkEditOption() {
        WebElement tableItinerary = WebDriverFactory.getDriver().findElement(byFirstLeg);
        WebElement firstLeg = tableItinerary.findElements(byTr).get(0);
        WebElement lastColumn = firstLeg.findElements(byTd).get(8);
        int checkEditAsThirdSpan = lastColumn.findElements(byCheckEdit).size();
        return checkEditAsThirdSpan;
    }

    public WebElement getEditOption() {
        WebElement tableItinerary = WebDriverFactory.getDriver().findElement(byFirstLeg);
        WebElement firstLeg = tableItinerary.findElements(byTr).get(0);
        WebElement lastColumn = firstLeg.findElements(byTd).get(8);
        //Edit option
        WebElement editOption = lastColumn.findElements(bySpan).get(5);
        return editOption;
    }

    public WebElement checkSetupItineraryPage() {
        return WebDriverFactory.getDriver().findElements(byHeadingText).get(0);
    }

    public void selectPreference(String emailTO, String emailCC, String emailText) throws InterruptedException {
        WebDriverFactory.getDriver().findElement(byPreferenceListDD).click();
        List<WebElement> preferenceList = WebDriverFactory.getDriver().findElements(byPreferenceList);
        preferenceList.get(0).click();
        Thread.sleep(2000);

        //resolve
        WebDriverFactory.getDriver().findElements(byResolve).get(1).click();

        //add email text
        getEmailTextField().sendKeys(emailText);

        //create itinerary
        WebDriverFactory.getDriver().findElements(byCreateItineraryButton).get(0).click();
        Thread.sleep(15000);

        WebDriverFactory.getDriver().findElements(bySelectAdd).get(0).click();
        WebDriverFactory.getDriver().findElement(byAdd).sendKeys(emailTO);
        Thread.sleep(2000);

        WebDriverFactory.getDriver().findElements(bySelectAdd).get(0).click();
        WebDriverFactory.getDriver().findElement(byAdd).sendKeys(emailCC);

        Thread.sleep(2000);
    }

    public WebElement saveAndExitButton() {
        //save & exit
        WebElement saveAndExitButton = WebDriverFactory.getDriver().findElements(bySaveAndExit).get(1);
        return saveAndExitButton;

    }

    public void checkRecipient(String emailTO, String emailCC){
        Boolean existTO = WebDriverFactory.getDriver().findElements(byCheckList).get(0).getText().contains(emailTO);
        Boolean existCC = WebDriverFactory.getDriver().findElements(byCheckList).get(1).getText().contains(emailCC);

        Assert.assertEquals(existTO.booleanValue(), true);
        Assert.assertEquals(existCC.booleanValue(), true);
    }

    public WebElement getOpenFlightCentreButton() {
        return WebDriverFactory.getDriver().findElement(byFlightCentre);
    }

    public int getTabsCount() {
        //check number of tabs opened
        List<String> browserTabs = new ArrayList<String>(WebDriverFactory.getDriver().getWindowHandles());
        return browserTabs.size();
    }

    public String getCurrentUrl() {
        List<String> browserTabs = new ArrayList<String>(WebDriverFactory.getDriver().getWindowHandles());

        //switch to newly opened tab
        WebDriverFactory.getDriver().switchTo().window(browserTabs .get(1));
        return WebDriverFactory.getDriver().getCurrentUrl();
    }

    public static WebElement getCrewCheckBox(){
        return WebDriverFactory.getDriver().findElement(byCrewCheckBox);
    }

    public static WebElement getTailNumberCheckBox(){
        return WebDriverFactory.getDriver().findElement(byTailNumberCheckBox);
    }

    public static WebElement getCallsignCheckBox(){
        return WebDriverFactory.getDriver().findElement(byCallsignCheckBox);
    }

    public static WebElement getHanderAddressCheckBox(){
        return WebDriverFactory.getDriver().findElement(byHanderAddressCheckBox);
    }

    public static WebElement getActualACCheckBox(){
        return WebDriverFactory.getDriver().findElement(byActualACCheckBox);
    }

    public WebElement getPetCheckBox(){
        return WebDriverFactory.getDriver().findElement(byPetCheckBox);
    }

    public WebElement getFinalType(){
        return WebDriverFactory.getDriver().findElement(byFinalType);
    }

    public WebElement getFinalText(){
        return WebDriverFactory.getDriver().findElement(byFinalText);
    }

    public void selectType1() {
        WebDriverFactory.getDriver().findElement(byType1).click();
    }

    public void selectType2() {
        WebDriverFactory.getDriver().findElement(byType2).click();
    }

    public String selectType3() {
        WebDriverFactory.getDriver().findElement(byType3).click();
        String selectedType = WebDriverFactory.getDriver().findElement(byType3).getText();
        return selectedType;
    }

    public int[] checkCheckboxes() {
        int checkCountActual = 0;
        int checkCountExpected = 0;
        String isChecked = "";

        //table header
        WebElement tableHeader = WebDriverFactory.getDriver().findElement(byTableHead);
        WebElement headerCheckbox = tableHeader.findElements(byTr).get(0).findElements(byTh).get(0);

        String headerCheckName = headerCheckbox.findElement(bySpan).findElement(byLabel).findElement(bySpan).getAttribute("className");
        if (headerCheckName.contains("checked")) { checkCountActual++; }

        //table body
        WebElement tableBody = WebDriverFactory.getDriver().findElement(byTableBody);
        List<WebElement> bodyRows = tableBody.findElements(byTr);

        //+ 1 is for header checkbox
        checkCountExpected = bodyRows.size() + 1;

        //check checkbox for every row of table
        for (int i = 0; i < bodyRows.size(); i++)
        {
            isChecked = bodyRows.get(i).findElements(byTd).get(0).findElement(bySpan).findElement(byLabel).findElement(bySpan).getAttribute("className");
            if(isChecked.contains("checked")) { checkCountActual++; }
        }
        //return actual count of checkboxes and expected count of checkboxes
        return new int[] {checkCountActual, checkCountExpected};
    }

    public static WebElement deselectFirstLeg() {
        WebElement tableItinerary = WebDriverFactory.getDriver().findElement(byFirstLeg);
        WebElement firstLeg = tableItinerary.findElements(byTr).get(0);
        WebElement firstColumn = firstLeg.findElements(byTd).get(0);

        return firstColumn;
    }

    public WebElement customTextOption() {
        return WebDriverFactory.getDriver().findElement(byCustomOption);
    }

    public WebElement VIPCateringOption() {
        return WebDriverFactory.getDriver().findElement(byVIPOption);
    }

    public void addCustomText(String randomString) {
        WebDriverFactory.getDriver().findElement(byCustomMsgField).sendKeys(randomString);
    }


    public WebElement checkMultipleLegsSelected() {
        WebElement tableHeader = WebDriverFactory.getDriver().findElement(byTableHeader);
        WebElement checkAll = tableHeader.findElements(byTh).get(0);
        return checkAll;
    }

    public void selectAndAddCustomText(String s) {
        WebDriverFactory.getDriver().findElement(byCateringPopUp).click();
        WebDriverFactory.getDriver().findElements(byInput).get(4).sendKeys(s);
        //click ok
        WebDriverFactory.getDriver().findElement(byCateringPopUpButton).click();
    }

    public WebElement checkCustomTextOfFirstLeg() {
        return WebDriverFactory.getDriver().findElements(byInput).get(4);
    }

    public void checkCustomItem() {
        WebElement tableItinerary = WebDriverFactory.getDriver().findElement(byTableBody);
        List <WebElement> rows = tableItinerary.findElements(byTr);
        int rowsCount = rows.size();

        for (int i = 0; i< rowsCount ; i++) {
            Assert.assertTrue(rows.get(i).findElements(byTd).get(8).getText().contains("Custom"));
        }
    }

    public String getGreetings() {
        return WebDriverFactory.getDriver().findElement(byGreetings).getAttribute("value");
    }

    public WebElement getEmailTextField() {
        return WebDriverFactory.getDriver().findElement(byEmailText);
    }

    public void checkEmailText(String randomStringForEmail) {
        Assert.assertTrue(WebDriverFactory.getDriver().findElement(byEmailText).getText().contains(randomStringForEmail));
    }

    public WebElement getSubjectField() {
        return WebDriverFactory.getDriver().findElement(bySubject);
    }

    static String[] selectedCustomItems = new String[2];
    static int legsCount = 0;

    public void applyCustomAdditionalItems() {

        WebElement tableItinerary = WebDriverFactory.getDriver().findElement(byTableBody);
        List <WebElement> rows = tableItinerary.findElements(byTr);
        int rowsCount = rows.size();
        legsCount = rowsCount;

        for (int i = 0; i < rowsCount; i++) {
            WebDriverFactory.getDriver().findElements(By.className("ant-dropdown-link")).get(i).click();
            List<WebElement> Labels = WebDriverFactory.getDriver().findElements(By.className("filter-menu")).get(i).findElements(By.tagName("label"));

            for (int j = 0; j<Labels.size(); j++) {
                if(Labels.get(j).findElement(By.tagName("span")).findElement(By.tagName("input")).isSelected())
                {
                    Labels.get(j).click();
                }
            }
            Labels.get(0).click();
            selectedCustomItems[0] = (Labels.get(0).getText());
            Labels.get(2).click();
            selectedCustomItems[1] = (Labels.get(2).getText());

            WebDriverFactory.getDriver().findElement(By.className("ant-card-body")).click();
        }
    }
}
