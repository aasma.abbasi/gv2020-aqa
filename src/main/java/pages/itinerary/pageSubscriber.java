package pages.itinerary;

import core.WebDriverFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.IPages.ISubscriber;
import pages.Page;

import java.util.List;

public class pageSubscriber implements ISubscriber{

    public pageSubscriber(Page page){

    }

    int historyRowsCount = 0;

    //Locators
    private By bySubscriberBtn = By.id("subscribers-btn");
    private By byAddSubscriberBtn = By.className("ant-card-extra");
    private By byAddSubscriberNameText = By.className("ant-select-search__field");
    private By byAddSubscriberDropdown = By.className("ant-select-dropdown-menu");
    private By byLi = By.tagName("li");
    private By bySpan = By.tagName("span");
    private By byButton = By.tagName("button");
    private By byAddSubscriberEmailField = By.className("ant-input");
    private By byAddSubscriberItineraryChckbx = By.className("ant-checkbox-group");
    private By byAddSubscriberItineraryChckbxField = By.className("ant-checkbox-input");
    private By byAddSubscriberScreen = By.className("ant-modal-footer");
    private By byAddSubscriberAddBtn = By.className("ant-btn");
    private By bySubscriberList = By.className("subscribers-list");
    private By bySubscriberTag = By.className("subscriber-tag");
    private By bySubscriberRemoveIcon = By.className("anticon-cross");
    private By bySubscriberReplicate= By.className("subscribers-replicate");
    private By bySubscriberBtns= By.className("subscribers-btns");
    private By byFieldname = By.className("normal-label");
    private By byperson = By.className("ant-select-search__field__wrap");
    private By byInput = By.tagName("input");
    private By byAllEmailFields = By.className("ant-radio-group");
    private By byLabel = By.tagName("label");
    private By byAllItineraryTypes = By.className("ant-checkbox-group");
    private By byUpdate = By.cssSelector(".ant-modal-footer > button");
    private By byFirstLeg = By.className("ant-table-tbody");
    private By byTr = By.tagName("tr");
    private By byNotification = By.className("ant-notification-notice-message");


    //Functions

    @Override
    public WebElement getSubscriberButton(){
        return WebDriverFactory.getDriver().findElement(bySubscriberBtn);
    }

    @Override
    public WebElement getAddSubscriberButton(){
        return WebDriverFactory.getDriver().findElements(byAddSubscriberBtn).get(1);
    }

    @Override
    public WebElement getAddSubscriberNameTxtbx(){
        return WebDriverFactory.getDriver().findElement(byAddSubscriberNameText);
    }

    public WebElement getAddSubscriberfirstName(){
        return WebDriverFactory.getDriver().findElement(byAddSubscriberDropdown).findElements(byLi).get(0);
    }

    public WebElement getExistingSubscriberfirstName(){
        return WebDriverFactory.getDriver().findElement(byAddSubscriberDropdown).findElements(byLi).get(0);
    }


    @Override
    public WebElement getAddSubscriberEmailTxtbx(){
        return WebDriverFactory.getDriver().findElements(byAddSubscriberEmailField).get(0);
    }

    @Override
    public WebElement getAddSubscriberEmailFieldRadio(){
        return WebDriverFactory.getDriver().findElements(byAddSubscriberEmailField).get(1);
    }

    @Override
    public WebElement getAddSubscriberItineraryTypeCheckbx(String itineraryType){

        int counter = 0;
        switch (itineraryType)
        {
            case "Initial":
                counter = 0;
                break;

            case "Final":
                counter = 1;
                break;

            case "Revised":
                counter = 2;
                break;
        }

        return WebDriverFactory.getDriver().findElement(byAddSubscriberItineraryChckbx).findElements(byAddSubscriberItineraryChckbxField).get(counter);
    }

    public WebElement getAddBtn(){
        return WebDriverFactory.getDriver().findElement(byAddSubscriberScreen).findElement(byAddSubscriberAddBtn);
    }

    public WebElement getSubscriberListLastEmail(){

        int lastEmailSubscriberList = WebDriverFactory.getDriver().findElement(bySubscriberList).findElements(bySubscriberTag).size() - 1;
        return WebDriverFactory.getDriver().findElement(bySubscriberList).findElements(bySubscriberTag).get(lastEmailSubscriberList);
    }

    public String deleteFromFieldName() {
        return WebDriverFactory.getDriver().findElements(byFieldname).get(0).getText();
    }

    public int deleteFromFieldSize() {
        return WebDriverFactory.getDriver().findElement(bySubscriberList).findElements(bySubscriberTag).size();
    }

    public WebElement getSubscriberListLastEmailRemoveIcon(){

        int lastEmailSubscriberList = WebDriverFactory.getDriver().findElement(bySubscriberList).findElements(bySubscriberTag).size() - 1;
        return WebDriverFactory.getDriver().findElement(bySubscriberList).findElements(bySubscriberTag).get(lastEmailSubscriberList).findElement(bySubscriberRemoveIcon);
    }

    public int getSubscriberListEmailCount(){

        int subscriberList = WebDriverFactory.getDriver().findElement(bySubscriberList).findElements(bySubscriberTag).size();
        return subscriberList;
    }

    public WebElement getUpdateLegsCheckbox(){

        return WebDriverFactory.getDriver().findElement(bySubscriberReplicate).findElement(bySpan);
    }

    public WebElement getSaveBtn(){
        return WebDriverFactory.getDriver().findElement(bySubscriberBtns).findElements(byButton).get(1);
    }

    public void openLatestHistoryLog() {
        WebElement historyTable = WebDriverFactory.getDriver().findElement(By.className("history-logs"));
        List<WebElement> rows = historyTable.findElements(By.tagName("li"));
        rows.get(0).click();
    }

    public WebElement getNotification(){
        return WebDriverFactory.getDriver().findElement(byNotification);
    }


    public void checkIfDelete(String deleteFromFieldName, int size) {
        String check = "";
        //if the field(e.g. TO) contains more than 1 subscribers than when we delete one subscriber, the size will decrease by 1
        //else check the field name changes (if only 1 subscriber was there and we deleted it in TO, then first field name now would be CC)
        if(deleteFromFieldName() == deleteFromFieldName) {
            Assert.assertEquals(deleteFromFieldSize(), size - 1);
        }
        else {
            check = deleteFromFieldName();
            Assert.assertNotEquals(check, deleteFromFieldName);
        }
    }

    String emailType = "";
    String personName = "";
    String emailName = "";

    public void selectAndEditSubscriber() {
        List<WebElement> subscribersTag = WebDriverFactory.getDriver().findElements(bySubscriberList);

        //open subscriber
        subscribersTag.get(0).findElements(bySpan).get(0).click();

        //save person name
        personName = WebDriverFactory.getDriver().findElement(byperson).findElement(byInput).getAttribute("value");

        //save email address
        emailName = WebDriverFactory.getDriver().findElement(By.className("ant-input-disabled")).getAttribute("value");

        //email fields
        WebElement emailFields = WebDriverFactory.getDriver().findElement(byAllEmailFields);
        List<WebElement> fields = emailFields.findElements(byLabel);

        for(int i=0; i<fields.size(); i++) {
            if(!fields.get(i).getAttribute("class").contains("checked"))
            {
                //save last selected email type
                emailType = fields.get(i).getText();
                fields.get(i).click();
            }
        }

        //itinerary types
        WebElement itineraryTypes = WebDriverFactory.getDriver().findElement(byAllItineraryTypes);
        List<WebElement> itineraries = itineraryTypes.findElements(byLabel);

        for(int i=0; i<itineraries.size(); i++) {
            if(!itineraries.get(i).findElement(bySpan).getAttribute("class").contains("checked"))
            {
                itineraries.get(i).click();
            }
        }
        //update
        WebDriverFactory.getDriver().findElement(byUpdate).click();
    }

    public void isUpdated() {
        Boolean isUpdate = false;

        List<WebElement> listOfEmailTypes = WebDriverFactory.getDriver().findElements(byFieldname);
        for (int i=0; i<listOfEmailTypes.size(); i++)
        {
            if(listOfEmailTypes.get(i).getText().contains(emailType) && (listOfEmailTypes.get(i).getText().contains(emailName) || listOfEmailTypes.get(i).getText().contains(personName) ))
            {
                isUpdate = true;
                break;
            }
        }

        Assert.assertTrue("Subscriber updated!", isUpdate);
    }

    public void getsubscriberLeg(){
        List<WebElement> tableItinerary = WebDriverFactory.getDriver().findElement(byFirstLeg).findElements(byTr);

        for (int k=0 ; k<tableItinerary.size() ; k++){
            tableItinerary.get(k).click();
            List<WebElement> subscribersTag = WebDriverFactory.getDriver().findElements(bySubscriberList);

            if (subscribersTag.get(0).findElements(bySpan).size() > 0) {
                break;
            }
        }
    }

    public WebElement getFirstLeg(){
        WebElement etableItinerary = WebDriverFactory.getDriver().findElement(byFirstLeg);
        WebElement firstLeg = etableItinerary.findElements(byTr).get(0);
        return  firstLeg;
    }

    public int historyRowsCount() {
        WebElement historyTable = WebDriverFactory.getDriver().findElement(By.className("history-logs"));
        List<WebElement> rows = historyTable.findElements(By.tagName("li"));
        historyRowsCount = rows.size();
        return historyRowsCount;
    }

    public void openLatestHistoryLogRow() {
        WebElement historyTable = WebDriverFactory.getDriver().findElement(By.className("history-logs"));
        List<WebElement> rows = historyTable.findElements(By.tagName("li"));
        rows.get(0).click();
    }

    public void checkSubscriberAddedHistory(String addedSubs) {
        System.out.println(addedSubs);
        Assert.assertTrue(addedSubs.contains(WebDriverFactory.getDriver().findElements(By.className("history-box")).get(1).findElement(By.className("ant-tag-green")).getText()));
    }

    public void checkHistoryUpdated(String deleteFromFieldName, List<WebElement> allSubscribersList) {

        Assert.assertTrue(deleteFromFieldName.contains(WebDriverFactory.getDriver().findElements(By.className("history-box")).get(0).findElement(By.className("ant-tag-red")).getText()));
//

        List<WebElement> historyAfter = WebDriverFactory.getDriver().findElements(By.className("history-box")).get(1).findElements(By.className("ant-tag-has-color"));
        int count = historyAfter.size();

        for (int i=0; i<count; i++) {
            Assert.assertTrue(allSubscribersList.get(i).getText().equals(historyAfter.get(i).getText()));
        }
    }

    public void isHistoryLogsMaintained(int historyRowsCountBefore) {
        int historyRowsCountAfter = historyRowsCount();
        System.out.println("historyRowsCountAfter" + historyRowsCountAfter);
        System.out.println("historyRowsCountBefore" + historyRowsCountBefore+1);
        Assert.assertEquals(historyRowsCountAfter, historyRowsCountBefore+1);
    }

}
