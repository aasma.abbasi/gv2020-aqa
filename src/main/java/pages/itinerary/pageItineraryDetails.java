package pages.itinerary;

import core.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.IPages.IItineraryDetails;
import pages.Page;

import java.util.List;

public class pageItineraryDetails implements IItineraryDetails {

    public pageItineraryDetails(Page page){


    }

    //Locators:
    private By byPreferencesAppliedPersonName = By.className("preference-user");
    private By byCCEmailNewButton = By.cssSelector("#cc-field > td:nth-child(2) > div > #add-email-tag > div");
    private By byCCEmailField = By.cssSelector("#add-email-tag > input");
    private By byBCCEmailNewButton = By.cssSelector("#bcc-field > td:nth-child(2) > div > #add-email-tag > div");
    private By byBCCEmailField =   By.cssSelector("#add-email-tag > input");
    private By byEmailItinerary =   By.id("email-itinerary");
    private By byCCSubscribesList = By.cssSelector("#cc-field > td:nth-child(2) > div");
    private By getByCCSubscribeElement = By.cssSelector("#cc-field > td:nth-child(2) > div > div");
    private By getByBCCSubscribesElement = By.cssSelector("#bcc-field > td:nth-child(2) > div > div");
    private By byBCCSubscribesList = By.cssSelector("#bcc-field > td:nth-child(2) > div");

    private By byDiv = By.tagName("div");

    @Override
    public WebElement getPreferencesAppliedPersonName(){
        return WebDriverFactory.getDriver().findElement(byPreferencesAppliedPersonName);
    }

    @Override
    public WebElement newCCEmailButton(){
        return WebDriverFactory.getDriver().findElement(byCCEmailNewButton);
    }

    @Override
    public WebElement newCCEmailField(){
        return WebDriverFactory.getDriver().findElement(byCCEmailField);
    }

    @Override
    public WebElement newBCCEmailButton(){
        return WebDriverFactory.getDriver().findElement(byBCCEmailNewButton);
    }

    @Override
    public WebElement newBCCEmailField(){
        return WebDriverFactory.getDriver().findElement(byBCCEmailField);
    }

    @Override
    public WebElement emailItinerary(){
        return WebDriverFactory.getDriver().findElement(byEmailItinerary);
    }

    @Override
    public int ccSubscribesList(){
        return WebDriverFactory.getDriver().findElement(byCCSubscribesList).findElements(byDiv).size();
    }

    @Override
    public int bccSubscribesList(){
        return WebDriverFactory.getDriver().findElement(byBCCSubscribesList).findElements(byDiv).size();
    }

    @Override
    public List<WebElement> getCCEmail(){
        return (List<WebElement>) WebDriverFactory.getDriver().findElements(getByCCSubscribeElement);
    }

    @Override
    public List<WebElement> getBCCEmail(){
        return (List<WebElement>) WebDriverFactory.getDriver().findElements(getByBCCSubscribesElement);
    }

}
