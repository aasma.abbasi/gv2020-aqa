package pages.itinerary;

import core.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.IPages.IPreferences;
import pages.Page;

import java.util.List;

public class pagePreferences implements IPreferences {


    public pagePreferences(Page page) {

    }

    private static By byPreference = By.cssSelector(".order-number");
    private static By bycustomer = By.cssSelector(".ant-spin-container > div:nth-of-type(2) .name");
    private static By byaddsubs = By.cssSelector("a");
    private static By bySName = By.cssSelector("li .ant-select-search__field");
    private static By bysug = By.xpath("/html/body/div[4]/div/div/div/ul[@role='listbox']/li[@role='option']");
    private static By byradbut = By.className("ant-radio-input");
    private static By bycheck = By.cssSelector(".ant-checkbox-group [value='1']");
    private static By byadd = By.cssSelector(".ant-modal-footer .ant-btn-primary");
    private static By byassert = By.cssSelector("[for='cc'] .subscriber-tags span");
    private static By bysavebtn = By.id("save-preferences");
    private static By bylead = By.className("pref-item");
    private static By byPrefDropdown = By.className("ant-select-selection-selected-value");
    private static By bycross = By.cssSelector("[for='cc'] .subscriber-tags:nth-of-type(1) i");
    private static By bySubsList = By.cssSelector(".subscribers-list .ant-tag-text");
    private By byTimeFormat = By.className("ant-radio-group");
    private By byLabel = By.tagName("label");


    //@Override
    public WebElement getPreference() {
        return WebDriverFactory.getDriver().findElement(byPreference);
    }

    public WebElement getCustomer() {
// WebDriverWait cwait = new WebDriverWait(WebDriverFactory.getDriver(), 20);
// cwait.until(ExpectedConditions.visibilityOfElementLocated(bycustomer));
        return WebDriverFactory.getDriver().findElement(bycustomer);
    }

    @Override
    public WebElement addSubs() {
        return null;
    }


    public static String newTimeFormat = "";
    public static String oldTimeFormat = "";

    public String[] changeTimeformat() {
        WebElement update = null;
        String isChecked = "";
        List<WebElement> bodyLabels = WebDriverFactory.getDriver().findElements(byTimeFormat).get(0).findElements(byLabel);

        //String[] body = WebDriverFactory.getDriver().findElements(byTimeFormat).get(0).findElements(byLabel);


        for (int i = 0; i < bodyLabels.size(); i++){
            isChecked = bodyLabels.get(i).getAttribute("className");
            if(!isChecked.contains("checked")) {
                update = bodyLabels.get(i);
                newTimeFormat = bodyLabels.get(i).getText();
            }
            else if (isChecked.contains("checked")){
                oldTimeFormat = bodyLabels.get(i).getText();
            }
        }
        update.click();
        return new String[] {oldTimeFormat, newTimeFormat};
    }

    public String checkTimeFormat() throws  InterruptedException{

        Thread.sleep(10000);
        List<WebElement> bodyLabels = WebDriverFactory.getDriver().findElements(byTimeFormat).get(0).findElements(byLabel);

        for (int i = 0; i < bodyLabels.size(); i++) {
            if (bodyLabels.get(i).getAttribute("className").contains("checked")) {
                newTimeFormat = bodyLabels.get(i).getText();
            }
        }
        return newTimeFormat;
    }

    public WebElement sname() {
//// WebDriverWait cwait = new WebDriverWait(WebDriverFactory.getDriver(), 20);
//// cwait.until(ExpectedConditions.visibilityOfElementLocated(bycustomer));
        return WebDriverFactory.getDriver().findElement(bySName);
    }

    public void sugname() {
//// WebDriverWait cwait = new WebDriverWait(WebDriverFactory.getDriver(), 20);
//// cwait.until(ExpectedConditions.visibilityOfElementLocated(bycustomer));
        WebDriverFactory.getDriver().findElements(bysug).get(0);

    }

    public WebElement eradio() {
//// WebDriverWait cwait = new WebDriverWait(WebDriverFactory.getDriver(), 20);
//// cwait.until(ExpectedConditions.visibilityOfElementLocated(bycustomer));
        return WebDriverFactory.getDriver().findElements(byradbut).get(1);
    }

    public WebElement itypecheck() {
//// WebDriverWait cwait = new WebDriverWait(WebDriverFactory.getDriver(), 20);
//// cwait.until(ExpectedConditions.visibilityOfElementLocated(bycustomer));
        return WebDriverFactory.getDriver().findElement(bycheck);
    }

    public WebElement subAdd() {
        return WebDriverFactory.getDriver().findElement(byadd);
    }

    public WebElement sassert() {
        return WebDriverFactory.getDriver().findElement(byassert);
    }

    public WebElement savebtn() {
        return WebDriverFactory.getDriver().findElement(bysavebtn);
    }

    public WebElement bydropdown() {
        return WebDriverFactory.getDriver().findElement(byPrefDropdown);
    }

    public WebElement lead() {
        List<WebElement> bodyLabels = WebDriverFactory.getDriver().findElements(bylead).get(0).findElements(byLabel);
        return WebDriverFactory.getDriver().findElement(bylead);
    }

    public WebElement cross() {
        return WebDriverFactory.getDriver().findElement(bycross);
    }

    public WebElement subList() {
        return WebDriverFactory.getDriver().findElement(bySubsList);
    }


}