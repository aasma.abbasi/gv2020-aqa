package pages.IPages;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface IItinerary {

//    static WebElement getFirstLeg();

    WebElement getCreateItineraryButton();

    WebElement selectItineraryType();

    List<WebElement> clickOnResolveButton();

    WebElement selectAllCheckboxButton();

    WebElement sendEmailItineraryButton();

    WebElement deSelectLegCheckBox();



    //  WebElement preferenceDropDown();
}
