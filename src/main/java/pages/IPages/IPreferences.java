package pages.IPages;

import org.openqa.selenium.WebElement;

public interface IPreferences {

        WebElement getPreference();
        WebElement getCustomer();
        WebElement addSubs();
        WebElement sname();
        void sugname();
        WebElement eradio();
        WebElement itypecheck();
        WebElement subAdd();
        WebElement sassert();
        WebElement savebtn();
        WebElement lead();
        WebElement bydropdown();
        WebElement cross();
        WebElement subList();

}
