package pages;

import org.openqa.selenium.WebElement;
import pages.itinerary.*;
import utils.LogHelper;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Page extends LogHelper {



    private final pageCatering pageCatering = new pageCatering(this);
    private final pageCommon pageCommon = new pageCommon(this);
    private final pageEmail pageEmail = new pageEmail(this);
    private final pageItinerary pageItinerary = new pageItinerary(this);
    private final pageLogin pageLogin = new pageLogin(this);
    private final pagePdf pagePdf = new pagePdf(this);
    private final pagePreferences pagePreferences = new pagePreferences(this);
    private final pageSubscriber pageSubscriber = new pageSubscriber(this);
    private final pageOrder pageOrder = new pageOrder(this);
    private final pageItineraryDetails pageItineraryDetails = new pageItineraryDetails(this);

    public Page() throws AWTException {
    }


    protected pageCatering getPageCatering() {return pageCatering;}
    protected pageCommon getPageCommon() {return pageCommon;}
    protected pageEmail getPageEmail() {return pageEmail;}
    protected pageItinerary getPageItinerary() {return pageItinerary;}
    protected pageLogin getPageLogin() {return pageLogin;}
    protected pagePdf getPagePdf() {return pagePdf;}
    protected pagePreferences getPagePreferences() {
        return pagePreferences;
    }
    protected pageSubscriber getPageSubscriber() {return pageSubscriber;}
    protected pageItineraryDetails getPageItineraryDetails() {return pageItineraryDetails;}
    protected pageOrder getPageOrder() {return pageOrder;}

}
