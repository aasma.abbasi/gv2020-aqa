package core;


import core.configuration.TestsConfig;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.net.MalformedURLException;
import java.sql.Timestamp;


public class WebDriverFactory {

    static WebDriver driver;
    static String device = TestsConfig.getConfig().getDevice();
    static String chromeDriverPath = device.equals("Windows") ? "driver/chromedriver.exe" : "driver/chromedriver";



    public static WebDriver getDriver() {

        if (driver != null) {
            return driver;
        } else {
            throw new IllegalStateException("Driver has not been initialized. " +
                    "Please call WebDriverFactory.startBrowser() before use this method");
        }
    }


    public static void startDriver() throws MalformedURLException {
        ChromeOptions op = new ChromeOptions();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        if(device .equals("linux"))
        {
            chromeDriverPath = "driver/chromedriver_linux";
        }

        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", chromeDriverPath);
            op.addArguments("--start-maximized");
//            op.addArguments("start-fullscreen");

            driver = new ChromeDriver(op);

/*
            System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
            driver = new ChromeDriver();
*/

        } else

        {
            throw new IllegalStateException("Driver has already been initialized. Quit it before using this method");
        }}


    /**
     * Finishes browser
     */
    public static void finishDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static File saveAllureScreenshot() {
      return null;
//        return driver.getScreenshotAs(OutputType.BYTES);
//        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    }


}
