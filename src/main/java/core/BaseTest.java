package core;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.net.MalformedURLException;

    public class BaseTest {

    @Before
    public void beforeTest() throws MalformedURLException {
        WebDriverFactory.startDriver();

    }

    @After
    public void afterTest(Scenario scenario) {
        System.out.println("Scenario :: " + scenario.getName() + " has finished with result :: " + scenario.getStatus() + "\n");
//        WebDriverFactory.finishDriver();
    }
}
