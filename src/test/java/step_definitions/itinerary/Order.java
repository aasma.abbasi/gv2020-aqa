package step_definitions.itinerary;

import Core.WebDriverWaits;
import core.WebDriverFactory;
import core.dbConnect.DbConnect_mySql;
import cucumber.api.java.en.*;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.Page;
import pages.Page;
import pages.itinerary.pageItinerary;

import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Order extends Page {

    static int totalOrderCount;

    public Order() throws AWTException {
    }

    @And("^\\[Order] Screen is opened$")
    public void verifyOrderScreen() throws InterruptedException, SQLException {
        logStep("Order Screen opened");
        getPageOrder().enterOrderId("391316");
//        getPageOrder().enterOrderId("144887");
//        getPageOrder().enterOrderId(DbConnect_mySql.getOrderId());
//        WebDriverWaits.waitUntilElementVisible(By.className("order-number"));
            Thread.sleep(5000);
//        WebDriverWaits.visibilityOfElementLocated(By.className("order-number"));
        Assert.assertTrue(getPageOrder().getOrderScreen().getText().contains("Order"));
    }

    @And("^\\[Order] user will be successfully able to get order with Order id '([^']*)'$")
    public void verifyOrderIdOnOrderScreen(String orderId) throws InterruptedException {
        logStep("Asserting Screen with Order ID");
        Thread.sleep(4000);
        Assert.assertTrue(getPageOrder().getOrderScreen().getText().contains(orderId));
    }

    @When("^\\[Order] User select any itinerary$")
    public void selectAnyItinerary() throws InterruptedException {
    }

    @And("^\\[Order] User click on Edit Copy$")
    public void clickEditCopy() throws InterruptedException {
        getPageOrder().clickEditCopy().click();
    }

    public String[] itineraryType;

    @And("^\\[Itinerary] User change the Itinerary Type on Setup Itinerary screen.$")
    public void updateItineraryType() throws InterruptedException {
        itineraryType = getPageOrder().updateItineraryType();
    }

    @Then("^\\[Itinerary] Type will be successfully selected$")
    public void checkItineraryTypeSelection() throws InterruptedException {
        logStep("Verify that Itinerary Type is successfully updated");
        Assert.assertNotEquals(itineraryType[0], itineraryType[1]);
        Assert.assertEquals(getPageOrder().checkItineraryTypeSelected(), itineraryType[1]);
    }

    @Then("^\\[Order] user will be successfully able to get search Leg with Order id$")
    public void getOrderScreenVerification() throws InterruptedException, SQLException {
        logStep("Asserting Order Screen");
        verifyOrderScreen();
    }

    @When("^\\[Order] User select itinerary with Sent state$")
    public void selectItineraryWithSentState() {
        WebDriverWaits.waitUntilElementVisible(By.className("success"));
//        WebDriverWaits.waitUntilElementVisible("status");
        logStep("Selecting itinerary with Ready state");
        getPageOrder().getItineraryType("Sent");
    }

    @Then("^\\[Order] User will be able to view Itinerary Details$")
    public void verifyItineraryDetailsScreen() {
        logStep("Verify Itinerary Details screen");
        Assert.assertEquals("Itinerary Details", getPageOrder().getOrderHeading().getText());
    }

    @And("^\\[Order] User will be unable to edit Itinerary$")
    public void verifyUnableToEditItinerary() {
        logStep("Verify Itinerary Details screen not editable");

        Assert.assertFalse(Boolean.parseBoolean(getPageOrder().getToField().getAttribute("input")));
        Assert.assertFalse(Boolean.parseBoolean(getPageOrder().getEmailSubjectField().getAttribute("input")));
        Assert.assertFalse(Boolean.parseBoolean(getPageOrder().getEmailBodyField().getAttribute("input")));
    }

    @And("^\\[Order] User must have '([^']*)' legs$")
    public void verifyLegs(String legType) {
        logStep("Verify " + legType + " Legs");
        if(legType.equals("multiple")) {
            totalOrderCount = getPageOrder().getTotalOrderCount();
            boolean multipleLegCheck = totalOrderCount > 1;
            Assert.assertTrue(multipleLegCheck);
        }
        else if(legType.equals("single")) {
            totalOrderCount = getPageOrder().getTotalOrderCount();
            boolean multipleLegCheck = totalOrderCount == 1;
            Assert.assertTrue(multipleLegCheck);
        }
    }

//    @And("^\\[Order] Screen is opened$")
//    public void orderScreenOpened() throws InterruptedException {
////        getPageOrder().getOrderText().getText();
//        logStep("Order Screen is opened");
//       // String headingText = getPageOrder().getOrderHeading().getText();
//       // Assert.assertEquals(headingText, "Itinerary Details" );
//    }

    @When("^\\[Order] User select itinerary with Ready state$")
    public void selectReadyItinerary()  {
        logStep("Selecting itinerary with Ready state");
        getPageOrder().getItineraryType("Ready");
    }

    @Then("^\\[Order] User will be able to see applied preference on Order screen$")
    public void checkAppliedPreference() throws InterruptedException {
        logStep("Able to see applied preference");
        Assert.assertEquals(getPageOrder().checkPreferenceApplied().booleanValue(), false);
        Thread.sleep(3000);
    }

    @And("^\\[Order] User de-select leg$")
    public void deselectLeg() throws InterruptedException {
        logStep("Deselect leg");
        getPageOrder().deselectLeg();
    }

    @And("^\\[Order] User leave the Setup Itinerary screen without creating itinerary$")
    public void goToBackPage() throws InterruptedException {
        logStep("Go back");
        Thread.sleep(5000 );
        getPageOrder().backArrow().click();
    }

    @Then("^\\[Order] De-selected legs will be remembered in the system for the same user$")
    public void checkDeselectedLeg() {
        logStep("Check if deselected legs remembered");
        getPageItinerary().getCreateItineraryButton().click();
        getPageOrder().checkDeselectLeg();
    }

    @And("^\\[Order] User select legs$")
    public void selectLeg() {
        logStep("Select legs");
        //select all legs
        pages.itinerary.pageItinerary.checkMultipleLegs().click();
    }

    @And("^\\[Order] User de-select operational notes against selected leg$")
    public void deselectOperationalNotes() throws InterruptedException {
        logStep("Deselect operational notes");
        getPageOrder().deselectOperationalNote().click();
    }

    @And("^\\[Order] User apply custom additional items against selected legs$")
    public void selectAdditionalItems() throws InterruptedException {
        logStep("Check additional items");
        getPageOrder().selectAdditionalItems();
    }

    @Then("^\\[Order] De-selected operational notes will be remembered in the system for the same user$")
    public void checkDeselectOperationalNote() throws InterruptedException {
        logStep("Check if deselected operational notes remembered");
        getPageItinerary().getCreateItineraryButton().click();
        getPageOrder().checkDeselectedOperationalNote();
    }

    @Then("^\\[Order] Operational notes will be de-selected successfully$")
    public void checkDeselctedOperationalNotes() {
        logStep("Operational notes deselected");
        getPageOrder().checkDeselectedOperationalNote();
    }

    @Then("^\\[Order] Selected additional item against each leg will be remembered in system and persist for same user$")
    public void checkAdditionalItems() {
        logStep("Selected additional item remembered");
        getPageItinerary().getCreateItineraryButton().click();
        getPageOrder().checkAdditionalItems();
    }

    @Then("^\\[Order] User will be able to apply custom additional items against multiple leg$")
    public void additionalItemsApplied() {
        logStep("Select additional items");
        getPageOrder().checkAdditionalItems();
    }

    @When("^\\[Order] User select any Final itinerary type$")
    public void selectFinalItinerary() throws InterruptedException {
        logStep("Selecting itinerary with Final type");
        getPageOrder().getFinalTypeItinerary();
    }

    @When("^\\[Order] User select any Initial itinerary type$")
    public void selectInitialItinerary() throws InterruptedException {
        logStep("Selecting itinerary with Initial type");
        getPageOrder().getInitialTypeItinerary();
    }

    @When("^\\[Order] User select any Revised itinerary type$")
    public void selectRevisedItinerary() throws InterruptedException {
        logStep("Selecting itinerary with Revised type");
        getPageOrder().getRevisedTypeItinerary();
    }

    @And("^\\[Order] User performs action on attached link to RequestFile$")
    public void attachedPDFLink() {
        logStep("Open attached PDF");
        getPageOrder().getAttachedPdf().click();
    }

    @Then("^\\[Order] User will be able to preview the ResponseFile$")
    public void checkFilePreview() {
        logStep("PDF file opened");
        int tabsCount = getPageItinerary().getTabsCount();
        Assert.assertEquals(2, tabsCount);
    }

    @But("^\\[Order] User has ResponseFile support on browser$")
    public void browserSupport() {
        logStep("Check browser support");
    }

    @And("^\\[Order] User is able to Preview the ServiceURL in a browser$")
    public void checkUrl() {
        logStep("PDF opened in new tab with new URL");
        String currentUrl = getPageItinerary().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(".pdf"));
    }
}
