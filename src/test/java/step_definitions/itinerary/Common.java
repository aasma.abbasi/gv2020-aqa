package step_definitions.itinerary;

import cucumber.api.java.en.When;
import pages.Page;

import java.awt.*;


public class Common extends Page {


    public Common() throws AWTException {
    }

    @When("^StepEnd ?(\\d+)?")
    public void stepEnd(String step){
        if(step!=null)
            logStep("StepEnd "+step,false);
        else
            logStep("StepEnd",false);
    }
}
