package step_definitions.itinerary;

import Core.WebDriverWaits;
import core.WebDriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.Page;

import java.awt.*;

public class Email extends Page {

    public Email() throws AWTException {
    }

    public Itinerary itineraryClass = new Itinerary();


    @Then("^\\[Email] User will be able to see selected itinerary Type on Send Email screen$")
    public void verifyItineraryType() {
        logStep("Verify Itinerary Type on Send Email screen");
        WebDriverWaits.waitUntilElementVisible(getPageEmail().checkEmailNotification());
//        WebDriverWaits.waitUntilElementVisible("ant-notification-notice-message");
        String type = getPageEmail().checkItineraryType().getText();
        Assert.assertEquals(type, itineraryClass.itineraryType);
    }

    @And("^\\[Email] User will be able to Create Itinerary of the selected Type$")
    public void verifyItineraryTypeUpdated() {
        logStep("Verify Itinerary Type Updated on Send Email screen");
        getPageItinerary().getCreateItineraryButton().click();

        WebDriverWaits.waitUntilElementVisible(getPageEmail().checkEmailNotification());
//        WebDriverWaits.waitUntilElementVisible("ant-notification-notice-message");
        String type = getPageEmail().checkItineraryType().getText();
        Assert.assertEquals(type, itineraryClass.itineraryType);
    }

    @Then("^\\[Email] Email will be generated in '([^']*)' form on Send Email screen.$")
    public void verifyEmailForm(String emailForm) {
//        WebDriverWaits.waitUntilElementVisible(getPageEmail().checkEmailNotification());
        WebDriverWaits.sleep(20000);
        logStep("Verify email is generated in " + emailForm + " form");
        if(emailForm == "tabular") {
            WebElement multipleLegCheck = getPageEmail().checkEmailBody();
//            Assert.assertFalse(Boolean.parseBoolean(getPageOrder().getToField().getAttribute("input")));
            Assert.assertTrue(Boolean.parseBoolean(multipleLegCheck.getAttribute("table")));
        }
        else if(emailForm == "descriptive") {
            WebElement multipleLegCheck = getPageEmail().checkEmailBody();
            Assert.assertFalse(Boolean.parseBoolean(multipleLegCheck.getAttribute("table")));
        }
    }

    public String cateringMessage = "Catering message as per Leg";
    @Then("^\\[Email] User will be able to see per leg catering message on Email screen$")
    public void verifyCateringMessage() {
        logStep("Verify catering message per leg");
        WebDriverWaits.waitUntilElementVisible(getPageEmail().checkEmailNotification());
        WebElement cateringMessageRow = getPageEmail().checkCateringMessageText();
        Assert.assertTrue(cateringMessageRow.getText().equals(cateringMessage));
    }
}
