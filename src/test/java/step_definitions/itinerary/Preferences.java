package step_definitions.itinerary;

import core.WebDriverFactory;
import Core.WebDriverWaits;
import core.configuration.TestsConfig;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.Page;

import java.awt.*;
import java.util.List;


public class Preferences extends Page {


    public Preferences() throws AWTException {
    }

    @And("^\\[Preference] screen is opened$")
    public void verifyOrderScreen() throws InterruptedException {
        logStep("preference Screen opened");
        Thread.sleep(5000);
        Assert.assertTrue(getPagePreferences().getPreference().getText().contains("Preferences"));
    }


    @And("^\\[Preference] screen should have Account$")
    public void preferenceScreenShouldHaveAccountS() throws InterruptedException {
        logStep("should have accounts");
        Thread.sleep(2000);
        Assert.assertTrue(getPagePreferences().getCustomer().getText().contains("Staffordshire Charity"));
    }

    @When("^\\[Preference] user select Account$")
    public void preferenceUserSelectAccount() {
        logStep("select account");
        getPagePreferences().getCustomer().click();
    }

    @And("^\\[Preference] user add a subscriber$")
    public void preferenceUserAddASubscriberAddSubscriber() throws Throwable {
        logStep("add a subscriber");
        getPagePreferences().addSubs().click();
//        getPagePreferences().sname().click();
        Thread.sleep(3000);
    }


    @And("^\\[Preference] user fill and select all the fields$")
    public void preferenceUserFillAndSelectAllTheFields() throws Throwable {
        logStep("filled the fields");
        getPagePreferences().sname().sendKeys("Saddam");
        Thread.sleep(8000);
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ENTER).build().perform();

//        getPagePreferences().sugname();
        getPagePreferences().eradio().click();
        getPagePreferences().itypecheck().click();
        getPagePreferences().subAdd().click();
    }


    @Then("^\\[Preference] subscriber successfully added by CEM user$")
    public void subscriberSuccessfullyAddedByCEMUser() throws Throwable {

        logStep("validate a subscriber");
        WebDriverWaits.visibilityOfElementLocated(By.cssSelector(".subscriber-tag span"));
        Assert.assertTrue(getPagePreferences().sassert().getText().contains("Saddam Khan"));

        getPagePreferences().cross().click();
        Thread.sleep(5000);
        getPagePreferences().savebtn().click();
    }

    @And("^\\[Preference] user save the preferences Save Preferences$")
    public void preferenceUserSaveThePreferencesSavePreferences() throws Throwable {
        logStep("save a subscriber");
        //WebDriverWaits.waitUntilElementIsClickable(getPagePreferences().savebtn());
        Thread.sleep(5000);
        getPagePreferences().savebtn().click();

    }

    @Then("^\\[Preference] will be saved and persist for future for same$")
    public void checkItineraryTypeSelection() throws InterruptedException {
        logStep("Verify that Itinerary Type is successfully updated");
        Assert.assertNotEquals(timeFormat[0], timeFormat[1]);
        Assert.assertEquals(getPagePreferences().checkTimeFormat(), timeFormat[1]);
    }

    @And("^\\[Preference] screen should have Account with lead Pax$")

    public void preferenceScreenShouldHaveAccountWithLeadPax() throws Throwable {
        logStep("Have a lead pax");
        getPagePreferences().bydropdown().click();
        Assert.assertTrue(getPagePreferences().lead().isEnabled());
    }
    public String[] timeFormat;
    @And("^\\[Preference] Changing Time Format")

    public void updateTimeFormat() throws Throwable {
        logStep("changing time format");
        // Time format that is already selected will get changed to the other time format.
        timeFormat = getPagePreferences().changeTimeformat();
    }



    @When("^\\[Preference] user select Lead Pax from the drop down$")
    public void preferenceUserSelectLeadPaxFromTheDropDown() throws Throwable {
        logStep("select a subscriber");
      getPagePreferences().lead().click();

    }

    @And("^\\[Preference] user remove a Subscriber$")
    public void preferenceUserRemoveASubscriber() throws Throwable {
        logStep("remove a subscriber");

        getPagePreferences().addSubs().click();
        getPagePreferences().sname().click();
        getPagePreferences().sname().sendKeys("Saddam");
        Thread.sleep(8000);
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ENTER).build().perform();

        getPagePreferences().eradio().click();
        getPagePreferences().itypecheck().click();
        getPagePreferences().subAdd().click();

        Thread.sleep(6000);
        getPagePreferences().cross().click();
        //  Assert.assertEquals(false,a.contains("Saddam Khan"));

    }

    @Then("^\\[Preference] subscriber will be removed from the Lead pax preference$")
    public void preferenceSubscriberWillBeRemovedFromTheLeadPaxPreference() throws Throwable {
        logStep("removed a subscriber assertion");
        Thread.sleep(5000);
        Assert.assertEquals(false,getPagePreferences().subList().getText().contains("Saddam Khan"));

        // Assert.assertEquals(false,getPagePreferences().cross().isDisplayed());


    }
}

