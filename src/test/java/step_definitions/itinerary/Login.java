package step_definitions.itinerary;

import Core.WebDriverWaits;
import core.WebDriverFactory;
import core.configuration.TestsConfig;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import pages.Page;

import java.awt.*;

public class Login extends Page {

    public static String loginUrl = TestsConfig.getConfig().getLoginUrl();

    public Login() throws AWTException {
    }

    @When("^\\[Login] Being logged in as MS user$")
    public void loginAsMSUser() throws InterruptedException {
        logStep("Login into MS user");

//        http://192.168.209.198:3001/order/1/4
//        http://192.168.209.71:3001/order/1/
//        http://192.168.209.78:3001/order/1/
//        WebDriverFactory.getDriver().navigate().to("http://192.168.209.181:3001/order/1/");

//        WebDriverFactory.getDriver().navigate().to(loginUrl + "/order/361266");

        WebDriverFactory.getDriver().navigate().to(loginUrl + "/orders");

        WebDriverWaits.waitUntilElementVisible(By.id("logo"));
        getPageLogin().login();
        WebDriverWaits.waitUntilElementVisible(By.className("order-number"));

    }

    @Given("^\\[Login] Being logged in as CEM user$")
    public void beingLoggedInAsCEMUser() throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        logStep("Login into CEM user");
        WebDriverFactory.getDriver().navigate().to(loginUrl + "/preferences");
        getPageLogin().login();
    }



}