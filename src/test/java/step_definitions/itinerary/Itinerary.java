package step_definitions.itinerary;
import Core.WebDriverWaits;
import General.GenericFunctions;
import core.WebDriverFactory;
import core.dbConnect.DbConnect_mySql;
import cucumber.api.DataTable;
import cucumber.api.java.cs.A;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en_scouse.An;
import cucumber.runtime.java.StepDefAnnotation;
import gherkin.lexer.Th;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Page;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static step_definitions.itinerary.Order.totalOrderCount;

public class Itinerary extends Page {

    public String emailTO = "";
    public String emailCC = "";

    public Itinerary() throws AWTException {
    }


    @When("^\\[Itinerary] user open Search Itineraries screen$")
    public void clickOnSearchItineraryScreen() {
        logStep("Click on Search Itineraries screen");
        getPageItinerary().getItineraryModulePage().click();
    }

    @When("^\\[Itinerary] user enter '([^']*)' in search bar$")
    public void inputOrderId(String orderID) throws InterruptedException, SQLException {
        logStep("Entering search data");
        if(orderID.contains("Account")){
            getPageItinerary().getSearchField().sendKeys(DbConnect_mySql.getCustomerName());
        }
        else {
            getPageItinerary().getSearchField().sendKeys(orderID);
        }
    }

    @And("^\\[Itinerary] user set date range from Calender$")
    public void selectStartAndEndDate() throws InterruptedException {
        logStep("Entering Start and End Date");
        getPageItinerary().getCalendarIcon().click();
        getPageItinerary().getStartDateCalendarField().click();
        getPageItinerary().getBackButtonOnStartDate().click();
        getPageItinerary().getSelectStartDate().click();
        getPageItinerary().getNextMonthOnEndDate().click();
        Thread.sleep(3000);
        getPageItinerary().getSelectEndDate().click();
        Thread.sleep(3000);
    }

    @Then("^\\[Itinerary] user will be successfully able to select date range$")
    public void verifyCalendar() throws InterruptedException {
        logStep("Verify Calender Picket");
        Assert.assertTrue(getPageItinerary().getCalendarPicker().isDisplayed());
    }

    @Then("^\\[Itinerary] user will be able to see list of orders created against account$")
    public void verifyOrderList() throws InterruptedException {
        logStep("Verify Order List");
        Thread.sleep(3000);
        Assert.assertTrue(getPageItinerary().getOrderListAccountSearch().isDisplayed());
    }

    @Then("^\\[Itinerary] user will be able to see list of orders$")
    public void verifyListOfOrders() throws InterruptedException {
        logStep("Verify Order should exist");
        verifyOrderList();
    }

    @Then("^\\[Itinerary] user will be successfully able to Search orders with in the selected dates against Account$")
    public void verifyListOfOrdersUsingAccount() throws InterruptedException {
        logStep("Verify Order should exist");
        verifyOrderList();
    }

    @Then("^\\[Itinerary] user will be successfully able to Search orders with in the selected dates against Passenger$")
    public void verifyListOfOrdersUsingPassenger() throws InterruptedException {
        logStep("Verify Order should exist");
        verifyOrderList();
    }

    @And("^\\[Itinerary] user click on Go to$")
    public void clickOnGotoButton() {
        logStep("Click on Search Button");
        getPageItinerary().getGotoButton().click();
    }

    @When("^\\[Itinerary] user will be able to see Order id option selected by default in search bar$")
    public void verifySearchOrderOption() {
        logStep("Verifying default Option on Search bar");
        Assert.assertEquals("Order ID", getPageItinerary().getSearchOptionSelectedItem().getText());
    }

    @When("^\\[Itinerary] user select Leg id from drop down in search bar$")
    public void selectLegIdFromDropDown() throws InterruptedException {
        logStep("Click on Search Button");
        getPageItinerary().getSearchOptionSelectedItem().click();
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ENTER).build().perform();
    }

    @When("^\\[Itinerary] user select Account from drop down in search bar$")
    public void selectAccountFromDropDown() throws InterruptedException {
        logStep("Click on Search Button");
        getPageItinerary().getSearchOptionSelectedItem().click();
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ENTER).build().perform();
        Thread.sleep(5000);
    }

    @When("^\\[Itinerary] user select Passenger from drop down in search bar$")
    public void selectPassengerFromDropDown() throws InterruptedException {
        logStep("Click on Search Button");
        getPageItinerary().getSearchOptionSelectedItem().click();
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.ENTER).build().perform();
        Thread.sleep(5000);
    }


    @Then("^\\[Itinerary] user will be able to see Leg id option selected in search bar$")
    public void verifySearchLegOption() {
        logStep("Verifying default Option on Search bar");
        Assert.assertEquals("Leg ID", getPageItinerary().getSearchOptionSelectedItem().getText());
    }

    @Then("^\\[Itinerary] user will be able to see Account option selected in search bar$")
    public void verifySearchAccountOption() {
        logStep("Verifying default Option on Search bar");
        Assert.assertEquals("Account", getPageItinerary().getSearchOptionSelectedItem().getText());
    }

    @Then("^\\[Itinerary] user will be able to see Passenger option selected in search bar$")
    public void verifySearchPassengerOption() {
        logStep("Verifying default Option on Search bar");
        Assert.assertEquals("Passenger", getPageItinerary().getSearchOptionSelectedItem().getText());
    }


    @And("^\\[Itinerary] search button will be displayed$")
    public void verifySearchButton() {
        logStep("Verifying Search Button");
        Assert.assertEquals("Search", getPageItinerary().getSearchButtonText().getText());
    }

    @And("^\\[Itinerary] user click on Search$")
    public void clickOnSearchButton() throws InterruptedException {
        logStep("Click on Search Button");
//        Thread.sleep(6000);
        getPageItinerary().getSearchButton().click();
    }

    @And("^\\[Itinerary] Calender icon will be displayed$")
    public void verifyCalenderIcon() {
        logStep("Verifying Calender icon");
        Assert.assertTrue(getPageItinerary().getCalendarIcon().isDisplayed());

    }

    /*    @When("^\\[Itinerary] user opens page with the option shown as Create Itinerary$")
        public void clickCreateItinerary() {
            logStep("Click on itinerary button");
            getPageItinerary().create().click();
        }*/
    @When("^\\[Itinerary] User opens page with the option shown as Create Itinerary$")
    public void clickCreateItinerary() throws InterruptedException {
        logStep("Click on Create Itinerary Button");
        WebDriverWaits.waitUntilElementIsClickable(getPageItinerary().getCreateItineraryButton());
        getPageItinerary().getCreateItineraryButton().click();
    }

    @And("^\\[Itinerary] User select multiple legs$")
    public void selectMultipleLegs() throws InterruptedException {
        WebDriverWaits.waitUntilElementIsClickable(getPageItinerary().checkMultipleLegs());
        logStep("Select multiple legs");
        getPageItinerary().checkMultipleLegs().click();
    }

    String path = "C:\\Users\\Venturedive\\Desktop\\Objectives";

    @And("^\\[Itinerary] User clicks on Upload File button$")
    public void clickUploadFile() throws InterruptedException {
        Thread.sleep(3000);
        logStep("Upload file");
//        getPageItinerary().getUploadFile().sendKeys(path);
        getPageItinerary().getUploadFile().click();
    }

    @And("^\\[Itinerary] User select file from local systems$")
    public void selectFile() throws InterruptedException, AWTException {
        logStep("Select file to upload");
        getPageItinerary().fileSelection();
    }

    @Then("^\\[Itinerary] User will be prompted to Upload files from a local system$")
    public void checkFilePromptText() throws InterruptedException, AWTException {
        WebDriverWaits.waitUntilElementVisible(By.className("ant-message-success"));
//        WebDriverWaits.waitUntilElementVisible("ant-message-success");
        Thread.sleep(500);
        logStep("Able to see correct valid prompt text");
        String promptText = getPageItinerary().getUploadFilePromptText().getText();
        Assert.assertTrue(promptText.contains("file uploaded successfully"));
    }

    @And("^\\[Itinerary] User will be shown green highlighted colour$")
    public void checkFilePromptColour() throws InterruptedException, AWTException {
        logStep("Able to see correct valid prompt GREEN colour");
        String promptColour = getPageItinerary().getUploadFilePromptColour().getCssValue("color");
//        System.out.println("\n\n" + promptColour + "\n");
//        String hex = String.format("#%02x%02x%02x", 82,196,26);
//        System.out.println(hex + "\n\n");
        Assert.assertEquals("rgba(82, 196, 26, 1)", promptColour);
    }

    //    @Then("^\\[Itinerary] User will be prompted to Upload files from local system$")
//    public void selectFile() throws InterruptedException {
//        Thread.sleep(3000);
//        logStep("Upload file from local system");
//        getPageItinerary().checkFile().click();
//    }

    @And("^\\[Itinerary] User select Include Bespoke per flight catering details entered above option from message section$")
    public void selectBespokeCheck() throws InterruptedException {
//        WebDriverWaits.waitUntilElementIsClickable(getPageItinerary().checkBespoke());
        logStep("Include bespoke checkbox from catering");
        getPageItinerary().checkBespoke().click();
    }

    @Then("^\\[Itinerary] User will be able to view Edit option against selected legs$")
    public void checkEdit() throws InterruptedException {
        logStep("Able to see edit option");
        Thread.sleep(3000);
        int spansCount = getPageItinerary().checkEditOption();
        Assert.assertEquals(4, spansCount);
    }

    @And("^\\[Itinerary] User click on Edit option against leg$")
    public void clickOnEditOfFirstLeg() throws InterruptedException {
        logStep("Click on edit");
        WebDriverWaits.waitUntilElementIsClickable(getPageItinerary().getEditOption());
        getPageItinerary().getEditOption().click();
    }

    @And("^\\[Itinerary] Setup itinerary page is opened$")
    public void checkSetupItineraryPage() {
        logStep("Setup itinerary page opened");
        String headingText = getPageItinerary().checkSetupItineraryPage().getText();
        Assert.assertEquals("Setup Itinerary", headingText);
    }

    @And("^\\[Itinerary] User add recipient in To , CC$")
    public void addRecipient(DataTable dt) throws InterruptedException {
        logStep("Add recipient");
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);

        emailTO = list.get(0).get("TO");
        emailCC = list.get(0).get("CC");

        String emailText = GenericFunctions.generateRandomString();

        selectMultipleLegs();
        getPageItinerary().selectPreference(emailTO, emailCC, emailText);
    }

    @And("^\\[Itinerary] User click on Save & Exit$")
    public void clickSaveAndExit() {
        logStep("Save and exit");
        getPageItinerary().saveAndExitButton().click();
    }

    @Then("^\\[Itinerary] Recipient will be added against an Itinerary in To , CC$")
    public void checkRecipientAdded(DataTable dt) {
        logStep("Recipient added");
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);

        emailTO = list.get(0).get("TO");
        emailCC = list.get(0).get("CC");

        getPageItinerary().checkRecipient(emailTO, emailCC);
    }

    @And("^\\[Itinerary] User deselect PDF additional items$")
    public void deselectAdditionalItems() {
        logStep("Deselect PDF additional items");
        getPageItinerary().selectType3();
        getPageItinerary().getCrewCheckBox().click();
        getPageItinerary().getTailNumberCheckBox().click();
        getPageItinerary().getCallsignCheckBox().click();
        getPageItinerary().getHanderAddressCheckBox().click();
        getPageItinerary().getActualACCheckBox().click();
        getPageItinerary().getPetCheckBox().click();
    }

    @And("^\\[Itinerary] User would be allowed to deselect PDF items$")
    public void verifyDeselectItemsCheck() {
        logStep("Verfying de-select checkbox");
        boolean enabled =
                getPageItinerary().getCrewCheckBox().isEnabled() &&
                        getPageItinerary().getTailNumberCheckBox().isSelected() &&
                        getPageItinerary().getCallsignCheckBox().isSelected() &&
                        getPageItinerary().getHanderAddressCheckBox().isSelected() &&
                        getPageItinerary().getActualACCheckBox().isSelected();
        Assert.assertFalse(enabled);
    }

    @And("^\\[Itinerary] User select PDF additional items$")
    public void selectAdditionalItems() {
        logStep("Select PDF additional items");
        getPageItinerary().getCrewCheckBox().click();
        getPageItinerary().getTailNumberCheckBox().click();
        getPageItinerary().getCallsignCheckBox().click();
        getPageItinerary().getHanderAddressCheckBox().click();
        getPageItinerary().getActualACCheckBox().click();
        getPageItinerary().getPetCheckBox().click();
    }

    @And("^\\[Itinerary] User would be allowed to select PDF items$")
    public void verifySelectItemsCheck() {
        logStep("Verifying select checkbox items");
        boolean enabled =
                getPageItinerary().getCrewCheckBox().isSelected() &&
                        getPageItinerary().getTailNumberCheckBox().isSelected() &&
                        getPageItinerary().getCallsignCheckBox().isSelected() &&
                        getPageItinerary().getHanderAddressCheckBox().isSelected() &&
                        getPageItinerary().getActualACCheckBox().isSelected();
        Assert.assertTrue(enabled);
    }


    public Random rand = new Random();
    public String itineraryType;

    @And("^\\[Itinerary] User select itinerary Type$")
    public void clickOnItineraryType() throws InterruptedException {
        int min = 1;
        int max = 1;
        int randomNum = rand.nextInt((max - min) + 1) + min;

        switch(randomNum){
            case 1:
                logStep("Click on any Initial Tab");
                itineraryType = "Initial";
                getPageItinerary().selectType1();
                break;
            case 2:
                logStep("Click on any Revised Tab");
                itineraryType = "Revised";
                getPageItinerary().selectType2();
                break;
            case 3:
                logStep("Click on any Final Tab");
                itineraryType = "Final";
                getPageItinerary().selectType3();
                break;
        }
    }

    @And("^\\[Itinerary] User will be able to Create Itinerary of the selected Type$")
    public void creatingItinerary() throws InterruptedException {
        logStep("Click on Create Itinerary ");
        getPageItinerary().selectAllCheckboxButton().click();
        if(Boolean.parseBoolean(String.valueOf(getPageItinerary().clickOnResolveButton().size() > 1))) {
//        if(getPageItinerary().clickOnResolveButton()) {
            getPageItinerary().clickOnResolveButton().get(3).click();
        }
        getPageItinerary().selectPreference();

        //   getPageItinerary().getPrefenceList().get(0).click();

        getPageItinerary().getCreateItineraryButton().click();
        getPageItinerary().sendEmailItineraryButton().click();

    }

    @And("^\\[Itinerary] User will be able select all Itinerary$")
    public void selectAllItinerary() throws InterruptedException {
        logStep("Selecting All Itinerary");
        getPageItinerary().selectAllCheckboxButton().click();
    }

    @And("^\\[Itinerary] User de-select any leg$")
    public void deselectLeg() throws InterruptedException {
        logStep("Deselecting any leg");
        getPageItinerary().deSelectLegCheckBox().click();
        Thread.sleep(7000);
    }


    @And("^\\[Itinerary] De-selected leg will not be included during the itinerary creation$")
    public void creatingItineraryWithoutDeSelectedLeg() throws InterruptedException {
        if (totalOrderCount > 2) {
            if(Boolean.parseBoolean(String.valueOf(getPageItinerary().clickOnResolveButton().size() > 1))) {
//        if(getPageItinerary().clickOnResolveButton()) {
                getPageItinerary().clickOnResolveButton().get(3).click();
            }
            getPageItinerary().selectPreference();
            getPageItinerary().getCreateItineraryButton().click();
            getPageItinerary().sendEmailItineraryButton().click();
        } else {
            getPageItinerary().getCreateItineraryButton().click();
            getPageItinerary().sendEmailItineraryButton().click();
        }
    }

    @And("^\\[Itinerary] Changes will be saved and persist in future$")
    public void isChangePersist() {
        logStep("Changes saved");
    }

    @And("^\\[Itinerary] User click on Open Flight Centre$")
    public void openFlightCentre() throws InterruptedException {
        logStep("Click on open flight centre");
        getPageItinerary().getOpenFlightCentreButton().click();
        Thread.sleep(5000);
    }

    @Then("^\\[Itinerary] User will be moved to Flight Centre of the selected Order in the new tab$")
    public void flightCentreOpened() {
        logStep("Flight centre GV page opened");
        int tabsCount = getPageItinerary().getTabsCount();
        Assert.assertEquals(2, tabsCount);

        String currentUrl = getPageItinerary().getCurrentUrl();
        Assert.assertTrue(currentUrl.contains("VJET/#view"));
    }

    @And("^\\[Itinerary] MS user select Type$")
    public void selectType() throws InterruptedException {
        logStep("Select type");
        getPageItinerary().selectType1();
    }


    @And("^\\[Itinerary] Will be successfully selected$")
    public void getFinalTextOnScreen() {
        logStep("Assertion on Final Text");
        Assert.assertTrue(getPageItinerary().getFinalText().isDisplayed());
    }


    @Then("^\\[Itinerary] Type will open with different pre-selected and prefilled items$")
    public void checkItems() throws InterruptedException {
        logStep("Type will open with different selected items");
        int[] compareChecks;

        //for type initial
        compareChecks = getPageItinerary().checkCheckboxes();
        Assert.assertEquals(compareChecks[0], compareChecks[1]);

        //for type revised
        getPageItinerary().selectType2();
        compareChecks = getPageItinerary().checkCheckboxes();
        Assert.assertEquals(compareChecks[0], compareChecks[1]);

        //for type final
        getPageItinerary().selectType3();
        compareChecks = getPageItinerary().checkCheckboxes();
        Assert.assertNotEquals(compareChecks[0], compareChecks[1]);
    }

    @And("^\\[Itinerary] User select Custom text option from message section$")
    public void selectCustomText() {
        logStep("Select custom text option");
        getPageItinerary().customTextOption().click();
    }

    @Then("^\\[Itinerary] Custom text will be successfully applied$")
    public void checkCustomTextSelected() {
        logStep("Check Custom text selected");
        Assert.assertTrue(getPageItinerary().customTextOption().isSelected());
    }

    @Then("^\\[Itinerary] Custom text will be applied to all the selected legs$")
    public void checkCustomTextForAllLegs() {
        logStep("Check custom text applied for all the selected legs");
        Assert.assertTrue(getPageItinerary().checkMultipleLegsSelected().findElement(By.tagName("input")).isSelected());
        checkCustomTextSelected();
    }

    @And("^\\[Itinerary] User select VIP catering message option from message section$")
    public void selectVIPCatering() {
        logStep("Select VIP catering");
        getPageItinerary().VIPCateringOption().click();
    }

    @Then("^\\[Itinerary] VIP catering message will be successfully applied$")
    public void checkVIPCateringSelected() {
        logStep("Check VIP catering selected");
        Assert.assertTrue(getPageItinerary().VIPCateringOption().isSelected());
    }

    @And("^\\[Itinerary] User select any Message option from message section$")
    public void selectAnyMessage() {
        logStep("Select any message section");
        selectVIPCatering();
    }

    @Then("^\\[Itinerary] Selected message option will persist for the same user$")
    public void checkSelectedMessageOptionPersist() {
        logStep("Check selected message option persist");
        getPageItinerary().getCreateItineraryButton().click();
        checkVIPCateringSelected();
    }

    @Then("^\\[Itinerary] Custom text message option will persist for the same user$")
    public void checkCustomTextOptionPersist() {
        logStep("Check Custom text option persist");
        getPageItinerary().getCreateItineraryButton().click();
        checkCustomTextSelected();
    }

    @And("^\\[Itinerary] User will be able to edit the Message option$")
    public void editMessageOption() {
        logStep("Edit message option");
        selectCustomText();
        checkCustomTextSelected();
    }

    @And("^\\[Itinerary] User add Custom text$")
    public void addCustomText() {
        logStep("Add Custom text");
        getPageItinerary().addCustomText(GenericFunctions.generateRandomString());
    }

    @And("^\\[Itinerary] User will be able to edit the Custom text$")
    public void editCustomText() {
        logStep("Edit custom text");
        getPageItinerary().addCustomText(GenericFunctions.generateRandomString());
    }

    String s = GenericFunctions.generateRandomString();

    @And("^\\[Itinerary] User select Custom text and add Custom text$")
    public void selectAndAddCustomText() throws InterruptedException {
        logStep("Select and add custom text");
        WebDriverWaits.waitUntilElementVisible(By.className("ant-modal-content"));
        getPageItinerary().selectAndAddCustomText(s);
    }

    @Then("^\\[Itinerary] Custom text will persist for the same MS user$")
    public void checkCustomText() throws InterruptedException {
        logStep("Check custom text persist for the first leg by clicking edit option");
        getPageItinerary().getCreateItineraryButton().click();
        getPageItinerary().getEditOption().click();
        Thread.sleep(3000);
        Assert.assertTrue(getPageItinerary().checkCustomTextOfFirstLeg().getText().equals(s));
    }

    @Then("^\\[Itinerary] Custom text will be successfully added against leg$")
    public void checkCustomTextAdded() throws InterruptedException {
        Thread.sleep(3000);
        getPageItinerary().getEditOption().click();
        Thread.sleep(3000);
        Assert.assertTrue(getPageItinerary().checkCustomTextOfFirstLeg().getText().equals(s));
    }

    @And("^\\[Itinerary] User will be able to edit the Custom text of first leg$")
    public void editCustomTestOfFirstLeg() {
        logStep("Edit custom text of first leg");
        getPageItinerary().selectAndAddCustomText(GenericFunctions.generateRandomString());
    }

    @Then("^\\[Itinerary] User will be able to see custom additional item against each leg shown as Custom$")
    public void customItemDisplayed() {
        logStep("Custom additional item is displayed against each leg");
        getPageItinerary().checkCustomItem();
    }

    @Then("^\\[Itinerary] User will be able to see Greetings$")
    public void checkGreetings() {
        logStep("Check greetings displayed");
        Assert.assertTrue(getPageItinerary().getGreetings().length() > 0);
    }

    String randomEmailString = GenericFunctions.generateRandomString();

    @And("^\\[Itinerary] User edit Email text$")
    public void editEmailText() {
        logStep("Edit email text");
        getPageItinerary().getEmailTextField().sendKeys(randomEmailString);
    }

    @Then("^\\[Itinerary] Email text will be edited$")
    public void checkEmailTextEdited() {
        logStep("Check email text edited");
        Assert.assertTrue(getPageItinerary().getEmailTextField().getAttribute("value").contains(randomEmailString));
    }

    @Then("^\\[Itinerary] Edited Email text will be shown for same user$")
    public void checkEmailTextPersist() {
        logStep("Check email text persist");
        getPageItinerary().checkEmailText(randomEmailString);
    }

    @And("^\\[Itinerary] Changes in email text will be saved and persist in future$")
    public void isEmailTextChangesPersist() throws InterruptedException {
        logStep("Changes in email text persist in future");
        getPageOrder().backArrow().click();
        clickCreateItinerary();
        getPageItinerary().checkEmailText(randomEmailString);
    }

    String randomSubjectString = GenericFunctions.generateRandomString();

    @And("^\\[Itinerary] User edit Subject Format$")
    public void editSubject() {
        logStep("Edit subject");
        getPageItinerary().getSubjectField().sendKeys(randomSubjectString);
    }

    @Then("^\\[Itinerary] Edited Subject Format will be shown for same user$")
    public void subjectTextPersist() {
        logStep("Check subject persist");
        Assert.assertTrue(getPageItinerary().getSubjectField().getAttribute("value").contains(randomSubjectString));
    }

    @Then("^\\[Itinerary] Subject Format will be edited$")
    public void isSubjectEdited() {
        logStep("Check if subject edited");
        subjectTextPersist();
    }

    @And("^\\[Itinerary] Changes in Subject Format will be saved and persist in future$")
    public void isSubjectChangesPersist() {
        logStep("Check if subject format changes persist in future");
        getPageOrder().backArrow().click();
        getPageItinerary().getCreateItineraryButton().click();
        subjectTextPersist();
    }


    @And("^\\[Itinerary] User apply custom additional items against selected legs$")
    public void applyCustomAdditionalItems() {
        logStep("Apply custom additional items against legs");
        getPageItinerary().applyCustomAdditionalItems();
    }

    @And("^\\[Itinerary] User click on Create Itinerary$")
    public void createItinerary() {
        logStep("Create itinerary");
        //check if conflict resolve displayed
        if(Boolean.parseBoolean(String.valueOf(getPageItinerary().clickOnResolveButton().size() > 1))) {
//        if(getPageItinerary().clickOnResolveButton()) {
            getPageItinerary().clickOnResolveButton().get(2).click();
        }

        getPageItinerary().getCreateItineraryButton().click();
    }

    @And("^\\[Itinerary] User click on View button against each leg on Send Email screen$")
    public void viewAgainstEachLeg() throws InterruptedException {
        logStep("View against each leg on email screen");
        Thread.sleep(9000);
        WebDriverWaits.waitUntilElementVisible(By.id("to-field"));
        getPageEmail().viewsSize();
    }

    @Then("^\\[Itinerary] User will be able to see custom additional items against each leg$")
    public void isCustomChangesAppliedToEachLeg() throws InterruptedException {
        getPageEmail().viewAgainstEachLeg();
    }

//    @When("^\\[Itinerary] User opens page with the option shown as Create Itinerary$")
//    public void clickCreateItinerary() throws InterruptedException {
//        logStep("Click on Create Itinerary Button");
//        WebDriverWaits.waitUntilElementIsClickable(getPageItinerary().getCreateItineraryButton());
//        getPageItinerary().getCreateItineraryButton().click();
//    }

    @And("^\\[Itinerary] User click on Create Itinerary at Setup Itinerary$")
    public void clickOnCreateItinerary() throws InterruptedException {
        clickCreateItinerary();
    }

    @Then("^\\[Itinerary] User will be able to Create Itinerary$")
    public void creatItinerary() throws InterruptedException {
        logStep("Click on Create Itinerary ");
        WebDriverWaits.waitUntilElementVisible(getPageEmail().checkEmailNotification());
//        WebDriverWaits.waitUntilElementVisible("ant-notification-notice-message");
        Assert.assertTrue(getPageEmail().checkEmailBody().isDisplayed());


    }
}


