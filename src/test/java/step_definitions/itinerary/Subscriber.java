package step_definitions.itinerary;

import Core.WebDriverWaits;
import core.WebDriverFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import gherkin.lexer.Th;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.Page;
import utils.LogHelper.*;

import java.awt.*;
import java.util.List;
import java.util.Map;


public class Subscriber extends Page{

    public String email = "";

    public Subscriber() throws AWTException {
    }

    @And("^\\[Subscriber] User open subscriber screen through Subscriber$")
    public void openSubscriberScreen() throws InterruptedException {
        logStep("User click on subscriber button");
        //Thread.sleep(10000);
        WebDriverWaits.waitUntilElementIsClickable(getPageSubscriber().getSubscriberButton());
        getPageSubscriber().getSubscriberButton().click();
    }

    @When("^\\[Subscriber] User taps on Add subscriber button$")
    public void tapAddSubscriber() {
        logStep("Click on Add subscriber button");
        getPageSubscriber().getFirstLeg().click();
        getPageSubscriber().getAddSubscriberButton().click();
    }

    @When("^\\[Subscriber] User adds subscribers using following data$")
    public void addSubscriberUsingData(DataTable dt) throws InterruptedException {

        logStep("Input data");
        //Thread.sleep(3000);
        WebDriverWaits.waitUntilElementVisible(By.className("ant-modal-content"));
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        email = list.get(0).get("TO");

        //TO
        getPageSubscriber().getAddSubscriberNameTxtbx().sendKeys("ADHOC");
        Thread.sleep(5000);
        getPageSubscriber().getAddSubscriberfirstName().click();

        //Email
        getPageSubscriber().getAddSubscriberEmailTxtbx().sendKeys(email);

        //Greetings
        getPageSubscriber().getAddSubscriberEmailFieldRadio().sendKeys("Dear John");

        //Itinerary type
        getPageSubscriber().getAddSubscriberItineraryTypeCheckbx(list.get(0).get("Itinerary type")).click();
//      getPageSubscriber().getAddSubscriberItineraryTypeCheckbx(list.get(1).get("Itinerary type")).click();
//      getPageSubscriber().getAddSubscriberItineraryTypeCheckbx(list.get(2).get("Itinerary type")).click();

        //Add subscriber
        getPageSubscriber().getAddBtn().click();
        Thread.sleep(2000);
    }

    @When("^\\[Subscriber] User type existing user name$")
    public void addExistingUserWithData(DataTable dt) throws InterruptedException {

        Thread.sleep(3000);
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        email = list.get(0).get("TO");

        //TO
        Thread.sleep(5000);
        getPageSubscriber().getAddSubscriberNameTxtbx().sendKeys("Saddam");
        Thread.sleep(5000);
        getPageSubscriber().getExistingSubscriberfirstName().click();

        //Greetings
        getPageSubscriber().getAddSubscriberEmailFieldRadio().sendKeys("Dear Saddam");

        //Itinerary type
        getPageSubscriber().getAddSubscriberItineraryTypeCheckbx(list.get(0).get("Itinerary type")).click();
//      getPageSubscriber().getAddSubscriberItineraryTypeCheckbx(list.get(1).get("Itinerary type")).click();
//      getPageSubscriber().getAddSubscriberItineraryTypeCheckbx(list.get(2).get("Itinerary type")).click();
        Thread.sleep(5000);

        //Add subscriber
        getPageSubscriber().getAddBtn().click();
        Thread.sleep(5000);
    }


    @Then("^\\[Subscriber] User is able to add subscribers to the itinerary list per leg$")
    public void isSubscriberAddedInItinerary() throws InterruptedException {
        logStep("Validating the subscribers email");
        Thread.sleep(3000);
        Assert.assertEquals("Assertion", getPageSubscriber().getSubscriberListLastEmail().getText(), email);
    }

    int deleteFromFieldSize = 0;
    String deleteFromFieldName = "";
    int historyRowsCountBefore = 0;

    @When("^\\[Subscriber] User taps on cross button against subscribers listed$")
    public void removeSubscriber(DataTable dt) throws InterruptedException {
        logStep("Tap on cross to remove subscriber");
        Thread.sleep(3000);
        //store name and size of the field before deleting subscriber
        deleteFromFieldSize = getPageSubscriber().deleteFromFieldSize();

        if(deleteFromFieldSize < 1) {
            tapAddSubscriber();
            addSubscriberUsingData(dt);
            saveSubscriberList();
        }

        historyRowsCountBefore = getPageSubscriber().historyRowsCount();
        deleteFromFieldName = getPageSubscriber().deleteFromFieldName();

        getPageSubscriber().getSubscriberListLastEmailRemoveIcon().click();
    }

    @Then("^\\[Subscriber] User should be able to remove previously added subscribers$")
    public void isSubscriberRemoved() {
        Assert.assertEquals("", getPageSubscriber().getSubscriberListEmailCount(), 1);
    }

    @When("^\\[Subscriber] User selects checkbox to implement on all legs and Save$")
    public void selectUpdateAllLegsChckbx() {
        getPageSubscriber().getUpdateLegsCheckbox().click();
        getPageSubscriber().getSaveBtn().click();
    }

    List<WebElement> allSubscribersList ;

    @When("^\\[Subscriber] User Save subscriber list$")
    public void saveSubscriberList() throws InterruptedException {
        logStep("Click on Save button");

        WebDriverWaits.waitUntilElementIsClickable(getPageSubscriber().getSaveBtn());
        getPageSubscriber().getSaveBtn().click();
        Thread.sleep(3000);

        WebDriverWaits.waitUntilElementVisible(By.className("ant-notification-notice-message"));
//        WebDriverWaits.waitUntilElementVisible("ant-notification-notice-message");
        Assert.assertTrue(getPageSubscriber().getNotification().isDisplayed());

    }

    @When("^\\[Subscriber] Changes to itinerary list should persist on all legs$")
    public void isChangesPersistOnItinery() {
    }

    @And("^\\[Subscriber] User select any leg for Subscriber$")
    public void selectLeg() {
        logStep("Select any leg for subscriber");
        getPageSubscriber().getsubscriberLeg();
        historyRowsCountBefore = getPageSubscriber().historyRowsCount();
    }

    @Then("^\\[Subscriber] Will be removed against leg at Subscriber List$")
    public void isSubscriberRemovedFromList() throws InterruptedException {
        logStep("Subscriber removed");
        getPageSubscriber().checkIfDelete(deleteFromFieldName, deleteFromFieldSize);
    }

    @And("^\\[Subscriber] User select any Subscriber and edit the given fields$")
    public void editSubscriber(DataTable dt) throws InterruptedException {
        logStep("Select subscriber and edit");
        deleteFromFieldSize = getPageSubscriber().deleteFromFieldSize();

        if(deleteFromFieldSize < 1) {
            tapAddSubscriber();
            addSubscriberUsingData(dt);
            saveSubscriberList();
        }

        historyRowsCountBefore = getPageSubscriber().historyRowsCount();

        getPageSubscriber().selectAndEditSubscriber();
    }

    @Then("^\\[Subscriber] Will be updated against leg at Subscriber List$")
    public void checkUpdated(DataTable dt) {
        logStep("Subscriber updated");
        getPageSubscriber().isUpdated();
    }

    @And("^\\[Subscriber] User open latest History logs$")
    public void openLatestHistoryLog() {
        logStep("Open latest history log");
        //store all updated subscribers in  string
        allSubscribersList = WebDriverFactory.getDriver().findElements(By.className("subscriber-tag"));

        getPageSubscriber().openLatestHistoryLogRow();
    }

    @Then("^\\[Subscriber] Subscriber will be removed against leg at Subscriber List$")
    public void isHistoryUpdated() throws InterruptedException {
        logStep("Check in history if subscriber is removed");
        getPageSubscriber().checkHistoryUpdated(deleteFromFieldName, allSubscribersList);
    }

    @Then("^\\[Subscriber] Will be added up against leg at Subscriber List$")
    public void checkSubscriberAddedHistory() throws InterruptedException {
        logStep("Check in history if subscriber is added");
        System.out.println(email);
        getPageSubscriber().checkSubscriberAddedHistory(email);
    }

    @And("^\\[Subscriber] Changes are maintained in History logs$")
    public void historyLogsMaintained() {
        logStep("Changed are maintained in history logs");
        getPageSubscriber().isHistoryLogsMaintained(historyRowsCountBefore);
    }
}
