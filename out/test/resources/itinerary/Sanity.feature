Feature: Itinerary Module - Basic Flow

  @Test @GI_25
  Scenario: GI-25
  [auto] MS users should be able to manually add subscribers to the itinerary list per leg
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    And   [Subscriber] User open subscriber screen through Subscriber
    When  [Subscriber] User taps on Add subscriber button
    And   [Subscriber] User adds subscribers using following data
      | TO               | CC                | BCC               | Itinerary type |
      | AnyName@mail.com | AnyName1@mail.com | AnyName2@mail.com | Initial        |
    Then [Subscriber] User is able to add subscribers to the itinerary list per leg
    When [Subscriber] User taps on cross button against subscribers listed
    Then [Subscriber] User should be able to remove previously added subscribers
    When [Subscriber] User selects checkbox to implement on all legs and Save
    Then [Subscriber] Changes to itinerary list should persist on all legs

  @Test @GI_583
  Scenario: GI-583
  [auto] MS user will be able to select private dinning per leg message on create itinerary screen.
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select Include Bespoke per flight catering details entered above option from message section
    Then  [Itinerary] User will be able to view Edit option against selected legs


  @Test @GI_631
  Scenario: GI-631
  [auto] MS user will be able to see preference applied for a ready state itinerary
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Order] User select itinerary with Ready state
    Then  [Order] User will be able to see applied preference on Order screen


  @Test @GI_554
  Scenario: GI-554
  [auto] MS user will be able to adhoc recipient on send email screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] Setup itinerary page is opened
    And   [Itinerary] User add recipient in To , CC
      | TO          | CC          |
      | Raza@vd.com | Raza@vd.com |
    And   [Itinerary] User click on Save & Exit
    Then  [Itinerary] Recipient will be added against an Itinerary in To , CC
      | TO          | CC          |
      | Raza@vd.com | Raza@vd.com |
    And   [Itinerary] Changes will be saved and persist in future


  @Test @GI_812
  Scenario: GI-812
  [auto] MS user will be able to move to flight centre from open flight center on setup itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User click on Open Flight Centre
    Then  [Itinerary] User will be moved to Flight Centre of the selected Order in the new tab


  @Test @GI_489
  Scenario: GI-489
  [auto] Initial/Revised/Final Itinerary pre-selected items when creating an itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] MS user select Type
    Then  [Itinerary] Type will open with different pre-selected and prefilled items


  @Test @GI_515
  Scenario: GI-515
  [auto] MS user will be able to remove subscriber per leg at Subscriber screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Subscriber] User open subscriber screen through Subscriber
    And   [Subscriber] User select any leg for Subscriber
    And   [Subscriber] User taps on cross button against subscribers listed
    Then  [Subscriber] Will be removed against leg at Subscriber List


  @Test @GI_516
  Scenario: GI-516
  [auto] MS user will be able to edit subscriber per leg at Subscriber screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Subscriber] User open subscriber screen through Subscriber
    And   [Subscriber] User select any leg for Subscriber
    And   [Subscriber] User select any Subscriber and edit the given fields
      | Email Field | Itinerary Types |
      | CC          | Revised         |
    Then  [Subscriber] Will be updated against leg at Subscriber List
      | Itinerary Types | Email Field |
      | Revised         | CC          |


  @Test @GI_642
  Scenario: GI-642
  [auto] Legs de-selected by the MS user will be remembered in the system.
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User must have 'multiple' legs
    And   [Order] User de-select leg
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    Then  [Order] De-selected legs will be remembered in the system for the same user


  @Test @GI_639
  Scenario: GI-639
  [auto] Operational notes de-selected by the MS user will be remembered in the system
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User must have 'multiple' legs
    And   [Order] User select legs
    And   [Order] User de-select operational notes against selected leg
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    Then  [Order] De-selected operational notes will be remembered in the system for the same user


  @Test @GI_632
  Scenario: GI-632
  [auto] MS user will be able to de-select operational notes on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User must have 'multiple' legs
    And   [Order] User select legs
    And   [Order] User de-select operational notes against selected leg
    Then  [Order] Operational notes will be de-selected successfully


  @Test @GI_509
  Scenario: GI-509
  [auto] Custom additional item selected against each leg will be remembered in the system
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User must have 'multiple' legs
    And   [Order] User select legs
    And   [Order] User apply custom additional items against selected legs
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    Then  [Order] Selected additional item against each leg will be remembered in system and persist for same user


  @Test @GI_694
  Scenario: GI-694
  [auto] MS user will be able to apply custom additional item option against each leg on setup itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User must have 'multiple' legs
    And   [Order] User select legs
    And   [Order] User apply custom additional items against selected legs
    Then  [Order] User will be able to apply custom additional items against multiple leg


  @Test @GI_541
  Scenario: GI-541
  [auto] MS user is able to view Final itinerary PDF
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Order] User select any Final itinerary type
    And   [Order] User performs action on attached link to RequestFile
    Then  [Order] User will be able to preview the ResponseFile
    But   [Order] User has ResponseFile support on browser
    And   [Order] User is able to Preview the ServiceURL in a browser


  @Test @GI_539
  Scenario: GI-539
  [auto] MS user is able to view Revised itinerary PDF
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Order] User select any Revised itinerary type
    And   [Order] User performs action on attached link to RequestFile
    Then  [Order] User will be able to preview the ResponseFile
    But   [Order] User has ResponseFile support on browser
    And   [Order] User is able to Preview the ServiceURL in a browser

  @Test @GI_531
  Scenario: GI-531
  [auto] MS user is able to view Initial itinerary PDF
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Order] User select any Initial itinerary type
    And   [Order] User performs action on attached link to RequestFile
    Then  [Order] User will be able to preview the ResponseFile
    But   [Order] User has ResponseFile support on browser
    And   [Order] User is able to Preview the ServiceURL in a browser


  @Test @GI_605
  Scenario: GI-605
  [auto] MS user will be able to custom text message on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select Custom text option from message section
    Then  [Itinerary] Custom text will be successfully applied


  @Test @GI_602
  Scenario: GI-602
  [auto] MS user will be able to select VIP catering message on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select VIP catering message option from message section
    Then  [Itinerary] VIP catering message will be successfully applied


  @Test @GI_597
  Scenario: GI-597
  [auto] Message option selected by MS user while creating an itinerary will be remembered in the system
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select any Message option from message section
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    Then  [Itinerary] Selected message option will persist for the same user
    And   [Itinerary] User will be able to edit the Message option


  @Test @GI_596
  Scenario: GI-596
  [auto] Custom catering message applied while creating an itinerary will be remembered in the system
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select Custom text option from message section
    And   [Itinerary] User add Custom text
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    Then  [Itinerary] Custom text message option will persist for the same user
    And   [Itinerary] User will be able to edit the Custom text


  @Test @GI_593
  Scenario: GI-593
  [auto] MS user will be able to add custom text for the selected legs on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select Custom text option from message section
    Then  [Itinerary] Custom text will be applied to all the selected legs


  @Test @GI_592
  Scenario: GI-592
  [auto] Custom catering message edited against leg will be remembered in the system
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select Include Bespoke per flight catering details entered above option from message section
    And   [Itinerary] User click on Edit option against leg
    And   [Itinerary] User select Custom text and add Custom text
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    Then  [Itinerary] Custom text will persist for the same MS user
    And   [Itinerary] User will be able to edit the Custom text of first leg


  @Test @GI_588
  Scenario: GI-588
  [auto] MS user will be able to add custom catering message against leg on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User select Include Bespoke per flight catering details entered above option from message section
    And   [Itinerary] User click on Edit option against leg
    And   [Itinerary] User select Custom text and add Custom text
    Then  [Itinerary] Custom text will be successfully added against leg


  @Test @GI_643
  Scenario: GI-643
  [auto] MS user will be able to see custom additional item option against each leg on set itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    Then  [Itinerary] User will be able to see custom additional item against each leg shown as Custom


  @Test @GI_567
  Scenario: GI-567
  [auto] MS user will be able to see greetings on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    Then  [Itinerary] User will be able to see Greetings


  @Test @GI_564
  Scenario: GI-564
  [auto] Email text edited by MS user will be remembered in the system
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User edit Email text
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    And   [Itinerary] User opens page with the option shown as Create Itinerary
    Then  [Itinerary] Edited Email text will be shown for same user


  @Test @GI_563
  Scenario: GI-563
  [auto] Subject edited by MS user will be preserve in the system on leaving the screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User select legs
    And   [Itinerary] User edit Subject Format
    And   [Order] User leave the Setup Itinerary screen without creating itinerary
    And   [Itinerary] User opens page with the option shown as Create Itinerary
    Then  [Itinerary] Edited Subject Format will be shown for same user


  @Test @GI_562
  Scenario: GI-562
  [auto] MS user will be able to edit the Email text on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User edit Email text
    Then  [Itinerary] Email text will be edited
    And   [Itinerary] Changes in email text will be saved and persist in future


  @Test @GI_561
  Scenario: GI-561
  [auto] MS user will be able to edit the subject on create itinerary screen
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User select legs
    And   [Itinerary] User edit Subject Format
    Then  [Itinerary] Subject Format will be edited
    And   [Itinerary] Changes in Subject Format will be saved and persist in future


  @Test @GI_874
  Scenario: GI-874
  [auto] MS user will be able to view additional item against each leg on send email
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    When  [Itinerary] User opens page with the option shown as Create Itinerary
    And   [Order] User must have 'multiple' legs
    And   [Itinerary] User select multiple legs
    And   [Itinerary] User apply custom additional items against selected legs
    And   [Itinerary] User click on Create Itinerary
    And   [Itinerary] User click on View button against each leg on Send Email screen
    Then  [Itinerary] User will be able to see custom additional items against each leg


#  @Test @GI_537
#  Scenario: GI-537
#  [auto] Subscriber to be prepopulated at Subscriber screen as per account preferences
#  Given [Login] Being logged in as MS user
#  And   [Order] Screen is opened
#  When  [Subscriber] User open subscriber screen through Subscriber
#  And   [Subscriber] User select any leg for Subscriber
#  Then  [Subscriber] Subscriber will be prepopulated as per account preferences


  @Test @GI_536
  Scenario: GI-536
  [auto] MS user is able to see the removed subscriber history made to each leg
    Given [Login] Being logged in as MS user
    And   [Order] Screen is opened
    And   [Subscriber] User open subscriber screen through Subscriber
    And   [Subscriber] User select any leg for Subscriber
    When  [Subscriber] User taps on cross button against subscribers listed
      | TO               | CC                | BCC               | Itinerary type |
      | AnyName@mail.com | AnyName1@mail.com | AnyName2@mail.com | Initial        |
    And   [Subscriber] User Save subscriber list
    And   [Subscriber] User open latest History logs
    Then  [Subscriber] Subscriber will be removed against leg at Subscriber List
    And   [Subscriber] Changes are maintained in History logs




#########################################################################################################

  @Test @GI_654
  Scenario: GI-654
  [auto] MS user will be able select itinerary type .
    Given   [Login] Being logged in as MS user
    And     [Order] Screen is opened
    When    [Itinerary] User opens page with the option shown as Create Itinerary
    And     [Order] User must have 'multiple' legs
    And     [Itinerary] User select itinerary Type
    Then    [Itinerary] User will be able to Create Itinerary of the selected Type

  @Test @GI_641
  Scenario: GI-641
  [auto] MS user will be able to de-select any leg during itinerary creation.
    Given    [Login] Being logged in as MS user
    And      [Order] Screen is opened
    When     [Itinerary] User opens page with the option shown as Create Itinerary
    And      [Itinerary] User select itinerary Type
    And      [Order] User must have 'multiple' legs
    And      [Itinerary] User will be able select all Itinerary
    And      [Itinerary] User de-select any leg
    Then     [Itinerary] De-selected leg will not be included during the itinerary creation

  @Test @GI_575
  Scenario: GI-575
  [auto] MS user will able to edit [CC], [BCC] of ready state itinerary on order screen
    Given    [Login] Being logged in as MS user
    And      [Order] Screen is opened
    When     [Order] User select itinerary with Ready state
    And      [ItineraryDetails] user add adhoc recipient in CC field
    And      [ItineraryDetails] user add adhoc recipient in BCC field
    Then     [ItineraryDetails] Recipient will be added in CC and BCC
    And      [ItineraryDetails] user will be able to Email Itinerary

  @Test @GI_630
  Scenario: GI-630
  [auto] MS  users should be able to manually add subscribers to the itinerary list per leg
    Given  [Login] Being logged in as MS user
    And    [Order] Screen is opened
    When   [Order] User select itinerary with Sent state
    Then   [Order] User will be able to see applied preference on Order screen

  @Test @GI_611
  Scenario: GI-611
  [auto] User will be able to add PDF additional items while creating an itinerary
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Itinerary] User deselect PDF additional items
    Then        [Itinerary] User would be allowed to deselect PDF items

  @Test @GI_576
  Scenario: GI-576
  [auto] User will be able to add PDF additional items while creating an itinerary
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Itinerary] User select PDF additional items
    Then        [Itinerary] User would be allowed to select PDF items

  @Test @GI_534
  Scenario: GI-534
  [auto] MS user will be able to add adhoc user as a subscriber per leg at Subscriber screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Subscriber] User open subscriber screen through Subscriber
    And         [Subscriber] User taps on Add subscriber button
    And         [Subscriber] User adds subscribers using following data
      | TO                |  | Itinerary type |
      | John@vistajet.com |  | Initial        |
    Then        [Subscriber] User is able to add subscribers to the itinerary list per leg


  @Test @GI_517
  Scenario: GI-517
  [auto] MS user will be able to save subscriber per leg at Subscriber screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Subscriber] User open subscriber screen through Subscriber
    And         [Subscriber] User taps on Add subscriber button
    And         [Subscriber] User adds subscribers using following data
      | TO                |  | Itinerary type |
      | John@vistajet.com |  | Initial        |
    Then        [Subscriber] User is able to add subscribers to the itinerary list per leg
    And         [Subscriber] User Save subscriber list
  #Then        [Subscriber] User is able to add subscribers to the itinerary list per leg

  @Test @GI_518
  Scenario: GI-518
  [auto] Changes made by MS user should reflect on all legs at Subscriber screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Subscriber] User open subscriber screen through Subscriber
    And         [Subscriber] User taps on Add subscriber button
    And         [Subscriber] User adds subscribers using following data
      | TO                |  | Itinerary type |
      | John@vistajet.com |  | Initial        |
    When        [Subscriber] User selects checkbox to implement on all legs and Save
#  Then       [Subscriber] User is able to add subscribers to the itinerary list per leg
    Then        [Subscriber] Changes to itinerary list should persist on all legs


  @Test @GI_507
  Scenario: GI-507
  [auto] MS user will be able to add existing user as a subscriber per leg at Subscriber screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Subscriber] User open subscriber screen through Subscriber
    And         [Subscriber] User taps on Add subscriber button
    And         [Subscriber] User type existing user name
      | TO          |  | Itinerary type |
      | Saddam Khan |  | Initial        |
    Then        [Subscriber] User is able to add subscribers to the itinerary list per leg


  @Test @GI_674
  Scenario: GI-674
  [auto]        MS user will be able to see Order id option on search itineraries screen
    Given        [Login] Being logged in as MS user
    Then         [Itinerary] user will be able to see Order id option selected by default in search bar


  @Test @GI_678
  Scenario: GI-678
  [auto]       MS user will be able to search order on the basis of Order id
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user enter '62270' in search bar
    And         [Itinerary] user click on Go to
    Then        [Order] user will be successfully able to get order with Order id '62270'


  @Test @GI_681
  Scenario: GI-681
  [auto]      MS user will be able to select Leg id option on search itineraries screen
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Leg id from drop down in search bar
    Then        [Itinerary] user will be able to see Leg id option selected in search bar


  @Test @GI_682
  Scenario: GI-682
  [auto]       MS user will be able to search order on the basis of Leg id
    Given       [Login] Being logged in as MS user
    And         [Itinerary] user enter '63805' in search bar
    And         [Itinerary] user click on Go to
    Then        [Order] user will be successfully able to get search Leg with Order id


  @Test @GI_683
  Scenario: GI-683
  [auto]       MS user will be able to select account  option on search itineraries screen
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Account from drop down in search bar
    Then        [Itinerary] user will be able to see Account option selected in search bar
    And         [Itinerary] search button will be displayed
    And         [Itinerary] Calender icon will be displayed


  @Test @GI_686
  Scenario: GI-686
  [auto]      MS user will be able to see list of orders created against account
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Account from drop down in search bar
    And         [Itinerary] user enter 'Account' in search bar
    And         [Itinerary] user set date range from Calender
    And         [Itinerary] user click on Search
    Then        [Itinerary] user will be able to see list of orders created against account


  @Test @GI_684
  Scenario: GI-684
  [auto]      MS user will be able to set date  range on account search
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Account from drop down in search bar
    And         [Itinerary] user enter 'Account' in search bar
    And         [Itinerary] user set date range from Calender
    And         [Itinerary] user click on Search
    Then        [Itinerary] user will be successfully able to Search orders with in the selected dates against Account


  @Test @GI_687
  Scenario: GI-687
  [auto]      MS user will be able to select passenger option on search itineraries screen
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Passenger from drop down in search bar
    Then        [Itinerary] user will be able to see Passenger option selected in search bar
    And         [Itinerary] search button will be displayed
    And         [Itinerary] Calender icon will be displayed


  @Test @GI_688
  Scenario: GI-688
  [auto]      MS user will be able to set date range on  passenger search
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Passenger from drop down in search bar
    And         [Itinerary] user enter 'James' in search bar
    And         [Itinerary] user set date range from Calender
    Then        [Itinerary] user will be successfully able to select date range


  @Test @GI_689
  Scenario: GI-689
  [auto]       MS user will be able to see list of orders in which specific passenger is included
    Given       [Login] Being logged in as MS user
    When        [Itinerary] user select Passenger from drop down in search bar
    When        [Itinerary] user enter 'James' in search bar
    And         [Itinerary] user set date range from Calender
    And         [Itinerary] user click on Search
    Then        [Itinerary] user will be able to see list of orders


  @Test @GI_549
  Scenario: GI-549
  [auto]       MS user is able to upload files
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
#    When        [Itinerary] user enter '63795' in search bar
#    And         [Itinerary] user click on Search
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Itinerary] User clicks on Upload File button
    And         [Itinerary] User select file from local systems
    Then        [Itinerary] User will be prompted to Upload files from a local system
    And         [Itinerary] User will be shown green highlighted colour


  @Test @GI_572
  Scenario: GI-572
  [auto]       MS user will not be able to edit sent state itinerary
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Order] User select itinerary with Sent state
#  All	Initial	Revised	Final
    Then        [Order] User will be able to view Itinerary Details
    And         [Order] User will be unable to edit Itinerary

  @Test @GI_668
  Scenario: GI-668
  [auto]      MS user will be able see select itinerary type on send email screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Order] User must have 'multiple' legs
    And         [Itinerary] User select itinerary Type
    And         [Itinerary] User click on Create Itinerary
    Then        [Email] User will be able to see selected itinerary Type on Send Email screen

  @Test @GI_669
  Scenario: GI-669
  [auto]      MS user will be able to edit itinerary type while editing an itinerary
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Order] User select any itinerary
    And         [Order] User click on Edit Copy
    And         [Itinerary] User change the Itinerary Type on Setup Itinerary screen.
    Then        [Itinerary] Type will be successfully selected
    And         [Email] User will be able to Create Itinerary of the selected Type

  @Test @GI_665
  Scenario: GI-665
  [auto]      Multiple leg(s) is selected while creating an itinerary email will be generated in tabular form on send email screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Order] User must have 'multiple' legs
    And         [Itinerary] User select multiple legs
    And         [Itinerary] User click on Create Itinerary
# Need to add functionality/assertions in Then step
    Then        [Email] Email will be generated in 'tabular' form on Send Email screen.

  @Test @GI_644
  Scenario: GI-644
  [auto]      Single leg is selected while creating an itinerary email will be generated in the descriptive form on send email screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Order] User must have 'single' legs
    And         [Itinerary] User click on Create Itinerary
# Need to add functionality/assertions in Then step
    Then        [Email] Email will be generated in 'descriptive' form on Send Email screen.

  @Test @GI_598
  Scenario: GI-598
  [auto]      Private catering message per leg can be viewed by MS user on send email screen
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Itinerary] User select multiple legs
    And         [Itinerary] User select Include Bespoke per flight catering details entered above option from message section
    And         [Itinerary] User click on Edit option against leg
    And         [Itinerary] User select Custom text and add Custom text
    And         [Itinerary] User click on Create Itinerary
    Then        [Email] User will be able to see per leg catering message on Email screen

  @Test @GI_511
  Scenario: GI-511
  [auto] MS user is able to see the added subscriber history made to each leg
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Subscriber] User open subscriber screen through Subscriber
    And         [Subscriber] User select any leg for Subscriber
# Extra And - Not suggested in TC
    And         [Subscriber] User taps on Add subscriber button
    And         [Subscriber] User adds subscribers using following data
      | Name  | TO                | Greetings | Email Field | Itinerary type |
      | ADHOC | John@vistajet.com | Dear John | CC          | Initial        |
    And         [Subscriber] User Save subscriber list
    And         [Subscriber] User open latest History logs
    Then        [Subscriber] Will be added up against leg at Subscriber List
    And         [Subscriber] Changes are maintained in History logs

  @Test @GI_535
  Scenario: GI-535
  [auto]      MS user is able to see the updated subscriber history made to each leg(s)
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Subscriber] User open subscriber screen through Subscriber
    And         [Subscriber] User select any leg for Subscriber
    And         [Subscriber] User select any Subscriber and edit the given fields
      | TO               | CC                | BCC               | Itinerary type |
      | AnyName@mail.com | AnyName1@mail.com | AnyName2@mail.com | Initial        |
    And         [Subscriber] User Save subscriber list
    And         [Subscriber] User open latest History logs
    Then  [Subscriber] Will be updated against leg at Subscriber List
      | Itinerary Types | Email Field |
      | Revised         | CC          |
    And         [Subscriber] Changes are maintained in History logs


  @Test @GI-544
  Scenario: GI-544
  [auto] CEM user is able to add subscriber at  account preference  screen
    Given   [Login] Being logged in as CEM user
    And     [Preference] screen is opened
    And     [Preference] screen should have Account
    When    [Preference] user select Account
    And     [Preference] user add a subscriber
    And     [Preference] user fill and select all the fields
    And     [Preference] user save the preferences Save Preferences
    Then    [Preference] subscriber successfully added by CEM user


  @Test @GI-545
  Scenario: GI-545
  [auto] CEM user is able to add subscriber on lead pax preference screen
    Given   [Login] Being logged in as CEM user
    And     [Preference] screen is opened
    And     [Preference] screen should have Account with lead Pax
    When    [Preference] user select Lead Pax from the drop down
    And     [Preference] user add a subscriber
    And     [Preference] user fill and select all the fields
    And     [Preference] user save the preferences Save Preferences
    Then    [Preference] subscriber successfully added by CEM user


  @Test @GI-546
  Scenario: GI-546
  [auto] CEM user is able to remove a subscriber from lead pax preference
    Given   [Login] Being logged in as CEM user
    And     [Preference] screen is opened
    And     [Preference] screen should have Account with lead Pax
    When    [Preference] user select Lead Pax from the drop down
    And     [Preference] user remove a Subscriber
    And     [Preference] user save the preferences Save Preferences
    Then    [Preference] subscriber will be removed from the Lead pax preference


  @Test @GI-614
  Scenario: MS user will be able to change itinerary type through edit a copy
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Order] User select any itinerary
    And         [Order] User click on Edit Copy
    And         [Itinerary] User change the Itinerary Type on Setup Itinerary screen.
    Then        [Itinerary] Type will be successfully selected
    And          [Email] User will be able to Create Itinerary of the selected Type

    #Done by Aasma Abbasi
  #GI-553

  @Test @GI-553
  Scenario: MS user will be able to create an itinerary
    Given       [Login] Being logged in as MS user
    And         [Order] Screen is opened
    When        [Itinerary] User opens page with the option shown as Create Itinerary
    And         [Itinerary] User click on Create Itinerary at Setup Itinerary
    Then        [Itinerary] User will be able to Create Itinerary


    #Add proper assertion on send email screen

  @Test @GI-512
  Scenario:  CEM user is able to set time format at account preference and that will persist for future
    Given [login] Being logged in as CEM user
    And [Preference] screen is opened
    And [Preference] screen should have Account
    When [Preference] user select Account
    And  [Preference] Changing Time Format
    And [Preference] user save the preferences Save Preferences
    Then [Preference] will be saved and persist for future for same


#  Time Format |12 hours | 24 hours |
#    And [CEM] user save preferences from button shown as [Save Preferences]
#    Then [Time Format] will be saved and persist for future for same [Account]
#  Time Format | 12 hours | 24 hours |

  @Test @GI-526
  Scenario: CEM user is able to set time format at lead pax preference and that will persist for future

    Given  [Login] Being logged in as CEM user
    And    [Preference] screen is opened
    And    [Preference] screen should have Account with lead Pax
    When   [Preference] user select Account
    And    [Preference] user select Lead Pax from the drop down
    And    [Preference] Changing Time Format
    And    [Preference] user save the preferences Save Preferences
    Then   [Preference] will be saved and persist for future for same






